https://www.postgresql.org/docs/9.1/sql-createrole.html

https://www.digitalocean.com/community/tutorials/how-to-use-roles-and-manage-grant-permissions-in-postgresql-on-a-vps-2

SCHEMA:

```
\d → Veiem el tipus de schema que tenim a la bbdd
```

schema public → Subcontenidor que ens serveix per agrupar taules dins d’una base de dades.

Podem crear mes ‘schemas’.

IMPORTANT!!: Podem tenir dos objectes que és diguin iguals EN EL CAS DE QUE ESTIGUIN EN ‘SCHEMAS’ DIFERENTS!!!!!

```
\dn →namespace

\? → ajuda d’ordres postgres

training=# \dn
  List of schemas
  Name  |  Owner   
--------+----------
 public | postgres
(1 row)
```

CREACIÓ ‘SCHEMA’ AMB EL MATEIX NOM QUE EL NOSTRE USUARI:

```
scott=# SELECT session_user, current_user; →COMPROVEM USUARI ACTUAL

scott=# CREATE SCHEMA isx48062351;
CREATE SCHEMA

scott=# CREATE TABLE emp (
        id    INT
);
```

SAP QUE HA DE CREAR-LA AL NOU SCHEMA PERQUÈ HI HA UNA DIRECTIVA QUE ÉS DIU:

```
scott=# SHOW SEARCH_PATH;
   search_path
----------------------
"$user", public
```

ÉS UNA RUTA DE CERCA (BÚSQUEDA) D’OBJECTES, “SEARCH_PATH == PATH”, LLOC ON BUSCA ELS BINARIS DE LES ORDRES QUE “PIQUEM”

ENS DEIXARÀ CREAR LA TAULA ENCARA QUE JA EXISTÍA, ENS HA CAMBIAT LA ESTRUCTURA (SCHEMA) I ESTEM UTILITZANT EL NOU.

```
scott=# SELECT *
                FROM emp;
---------------------------------------------------------------------------------------------------------------------------
scott=# SHOW all;

scott=# SHOW search_path;
   search_path   
-----------------
 "$user", public
(1 row)
```

**SI HI HAN 2 SCHEMES, IRÀ AL PRIMER D’ELLS!!!!!!**

SELECCIONEM L’SCHEMA ‘PUBLIC’ EN COMPTES DEL NOSTRE SCHEMA:

```
scott=# SELECT * 
               FROM public.emp;
```

CREEM DOS NOUS CAMPS:

```
scott=# ALTER TABLE emp ADD nom VARCHAR(10);
ALTER TABLE

scott=# ALTER TABLE emp ADD deptno SMALLINT;
ALTER TABLE
```

Mostreu nom d’empleat del schema ‘isx…’ i nom del seu departament del schema ‘public’:

```
scott=# SELECT isx48062351.emp.nom, public.dept.dname 
                FROM isx48062351.emp 
                  JOIN public.dept 
                     ON isx48062351.emp.deptno = public.dept.deptno;
  nom   |   dname    
--------+------------
 Jenner | ACCOUNTING
(1 row)
```

ALTRA SINTÀXI EN EL CAS DE QUE SAPIGUEM QUE LA TAULA NO EXISTEIX EN L’SCHEMA (NO UTILITZAR, ES IMPORTANT FER SABER A L’USUARI O ADMINISTRADOR AMB QUIN SCHEMA ESTEM TREBALLANT!):

```
scott=# SELECT emp.nom, dept.dname
                FROM emp
                  JOIN public.dept
             ON emp.deptno = dept.deptno;
  nom   |   dname    
--------+------------
 Jenner | ACCOUNTING
(1 row)
```

HEM DE DONAR PERMISOS DE LECTURA (SELECT) DE TOTES LES TAULES DE L’SCHEMA ‘isx…’ AL ROL ‘prova’

```
scott=> GRANT SELECT ON ALL TABLES IN SCHEMA isx48062351 TO prova;
```
