**Accés a Postgres**

Per accedir al SGBD PostgreSql ens hem d'autenticar. Sigui des de psql o des d'una aplicació (per exemple un programa en java) que  accedeix a la base de dades.

PostgreSql permet diversos tipus d'autenticació **definits al fitxer /etc/postgresql/13/main/pg\_hba.conf (o /var/lib/pgsql/data/pg\_hba.conf** en versions antigues**)**

A cada línia d'aquest fitxer s'especifica:

- si s'accedeix localment o remotament
- a quines bases de dades es pot accedir
- quins usuaris hi poden accedir
- quines adreces (IP) poden accedir
- el mètode d'autenticació

Quan intentem accedir a la base de dades, la nostra connexió es compararà amb les línies del fitxer. I **s'aplicarà el mètode indicat a la primer línia que faci matching**. Per això l'ordre és molt important. S'han de posar de més restrictives a menys restrictives.

Sintaxi:

```
TYPE  DATABASE        USER            ADDRESS                 METHOD
```

ó

```
TYPE  DATABASE        USER            IP-ADDRESS      IP-MASK             METHOD
```

**Exemples:**

- Permet connexió local a qualsevol base de dades als usuaris del sistema 

```
local   all             all                                     peer
```

- Permet connexió local a qualsevol base de dades a qualsevol usuari sense posar contrasenya 

```
local   all             all                                     trust
```

- Accés local (però amb connexió TCP/IP) per a usuaris del sistema 

```
host    all             all             127.0.0.1/32            ident
```

- El mateix que l'anterior però usant màscara de xarxa

```
host    all             all             127.0.0.1       255.255.255.255     ident
```

- Permet connexió de qualsevol usuari de Postgres amb contrasenya des de la màquina amb IP 192.168.12.10 connectar-se a la base de dades training 

```
host    training        all             192.168.12.10/32        md5
```

- Rebutja tota connexió des de la màquina amb IP 192.168.54.1

```
host    all             all             192.168.54.1/32         reject (Per dir qualsevol adreça de xarxa: 0.0.0.0/0)
```

**A més, per fer accessos remots, hem de dir a Postgresql que escolti a les adreces de xarxa que volem en el fitxer /var/lib/pgsql/data/postgresql.conf**:

- Per defecte listen\_addresses='localhost'
- Per escoltar tot: listen\_addresses='\*'
- Per escoltar IPs determinades: listen\_addresses='192.168.0.24, 192.168.0.26'

**CADA COP que toquem fitxers de configuració hem de reiniciar el servei de Postgresql:**

```
- systemctl restart postgresql
```

**Accés**:

```
$ psql -h ip\_servidor|nom\_servidor -U nom\_usuari nom\_bd
```

**Mètodes d'autenticació: trust, peer, ident, md5, reject**

- Mètode d'autenticació **peer**: Deixa connectar-se en local si l'usuari de bases de dades existeix com usuari de SO. És el mètode amb el què ens hem estat validant fins arribar aquest tema.
- Mètode d'autenticació **ident**: Deixa connectar-se en remot si l'usuari de bases de dades existeix com usuari de SO.
- Mètode d'autenticació **md5**: Comprovar que l'usuari estigui a la base de dades i el password proporcionat coincideixi amb el definit a la BD. Per a provar aquest mètode d'autenticació caldrà que li posem password a l'usuari de base de dades (ara per ara no en té), configurar el mètode al fitxer corresponent i reiniciar servidor.
- Mètode d'autenticació **reject**: Afegiu una nova entrada al fitxer per denegar l'accés a un usuari en concret a una BBDD en concret.

Més info: [https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html ](https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html)**Més exemples:**

**Ejemplo 1**

Acceso por tcp/ip (red) a la base de datos test001, como usuario test desde el ordenador con IP 10.0.0.100, y método de autentificación md5:

```
host    test001   test  10.0.0.100/32  md5
```

**Ejemplo 2**

Acceso por tcp/ip (red) a la base de datos test001, como usuario test desde todos los ordenadores de la red 10.0.0.0, con mascara de red 255.255.255.0 (254 ordenadores en total) y método de autentificación md5:

```
host    test001   test  10.0.0.0/24  md5
```

**Ejemplo 3**

Acceso por tcp/ip (red), a todas las bases de datos de nuestro cluster, como usuario test desde el ordenador con IP 10.0.0.100, y el ordenador 10.1.1.100 y método de autentificación md5 (necesitamos dos entradas en nuestro fichero pg\_hba.conf):

```
host    all   test  10.0.0.100/32   md5 host    all   test  10.1.1.100/32   md5
```

**Ejemplo 4**

Denegar el acceso a todos las bases de datos de nuestro cluster al usuario test, desde todos los ordenadores de la red 10.0.0.0/24 y dar acceso al resto del mundo con el método md5:

```
host       all   test  10.0.0.0/24   reject host       all   all  0.0.0.0/0     md5
```
