# M02 - BBDD

## Enllaç a la documentació oficial de 'postgres':

https://www.postgresql.org/docs/current/

## Continguts UF3:

* DML
* DDL (+ Views)
* CASE
* Transaccions
* Concurrència i bloquejos
* DCL
* Backups
