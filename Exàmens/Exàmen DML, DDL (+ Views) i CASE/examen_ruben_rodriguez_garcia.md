# Examen DML, DDL (Views) i CASE

## Pregunta 1

Amb una única sentència SQL, establiu la quota a només 1 (1€) als treballadors menors de 35 anys i als treballadors de New York i pugeu un 5% a la resta. Pista: feu servir CASE

```
training=*> UPDATE rep_vendes
              SET quota = (CASE WHEN edat < 35
                                 AND oficina_rep IN (SELECT oficina
                                                       FROM oficines
                                                      WHERE ciutat = 'New York')
                                 AND quota IS NOT NULL
                                THEN 1
                                ELSE quota + quota * 0.05
                                END);
UPDATE 10

training=*> SELECT *
              FROM rep_vendes ;
 num_empl |      nom      | edat | oficina_rep |       carrec        | data_contracte | cap |   quota   |  vendes
----------+---------------+------+-------------+---------------------+----------------+-----+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Representant Vendes | 1988-02-12     | 104 | 367500.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Representant Vendes | 1989-10-12     | 106 |      1.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Representant Vendes | 1986-12-10     | 108 | 367500.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Vendes           | 1988-06-14     |     | 288750.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Vendes          | 1987-05-19     | 106 | 210000.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Representant Vendes | 1986-10-20     | 104 | 315000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Representant Vendes | 1990-01-13     | 101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Vendes          | 1989-10-12     | 106 | 367500.00 | 361865.00
      103 | Paul Cruz     |   29 |          12 | Representant Vendes | 1987-03-01     | 104 | 288750.00 | 286775.00
      107 | Nancy Angelli |   49 |          22 | Representant Vendes | 1988-11-14     | 108 | 315000.00 | 186042.00
(10 rows)
```

## Pregunta 2

Esborreu els representants de vendes que tenen unes vendes inferiors a 300000 i no són directors d'oficina ni caps d'empleats.

```
training=*> DELETE FROM rep_vendes AS dir
             WHERE vendes < 300000
               AND num_empl NOT IN (SELECT director
                                      FROM oficines)
               AND oficina_rep NOT IN (SELECT oficina
                                         FROM oficines
                                        WHERE director = dir.num_empl);
ERROR:  update or delete on table "rep_vendes" violates foreign key constraint "fk_clients_rep_clie" on table "clients"
DETAIL:  Key (num_empl)=(110) is still referenced from table "clients".

Ens dona un error d'integritat referencial ja que la columna 'num_empl' és 'Foreign Key' del camp 'rep_clie' de la taula 'clients' i del camp 'rep' de la taula 'comandes'.
Llavors, haurem d'eliminar les constraint per poder eliminar els representants de vendes:

training=*> ALTER TABLE clients
            DROP CONSTRAINT fk_clients_rep_clie;
ALTER TABLE

training=*> ALTER TABLE comandes
            DROP CONSTRAINT fk_comandes_rep;
ALTER TABLE

training=*> DELETE FROM rep_vendes AS dir
             WHERE vendes < 300000
               AND num_empl NOT IN (SELECT director
                                      FROM oficines)
               AND oficina_rep NOT IN (SELECT oficina
                                         FROM oficines
                                        WHERE director = dir.num_empl);
DELETE 3
```

## Pregunta 3

A la base de dades training afegiu un nou client amb:

* Nom d'empresa "Z Inc.".
* Ha de tenir el límit de crèdit igual al límit més petit de la resta de clients.

* El representant de vendes ha de ser aquell representant que tingui menys clients assignats, 
  en cas d'empat, s'assignarà el representant de vendes amb menys nombre de vendes (nombre de 
  comandes que ha atès).

* L'identificador del client ha de ser el següent valor després del valor més alt de la resta d'identificadors de clients.

* Suposarem que tots els venedors tenen al menys 1 client assignat, però podria haver algun venedor sense vendes.

```
training=*> INSERT INTO clients
            VALUES ((SELECT MAX(num_clie) + 1
                       FROM clients),
                     'Z Inc.',
                    (SELECT num_empl
                       FROM rep_vendes
                       JOIN clients
                         ON num_empl = rep_clie
                      WHERE vendes IS NOT NULL
                      GROUP BY num_empl
                      ORDER BY count(*), MIN(vendes)
                      FETCH FIRST 1 ROW ONLY),
                    (SELECT MIN(limit_credit)
                       FROM clients));
INSERT 0 1

training=*> SELECT *
              FROM clients;
 num_clie |      empresa      | rep_clie | limit_credit
----------+-------------------+----------+--------------
     2111 | JCP Inc.          |      103 |     50000.00
     2102 | First Corp.       |      101 |     65000.00
     2103 | Acme Mfg.         |      105 |     50000.00
     2123 | Carter & Sons     |      102 |     40000.00
     2107 | Ace International |      110 |     35000.00
     2115 | Smithson Corp.    |      101 |     20000.00
     2101 | Jones Mfg.        |      106 |     65000.00
     2112 | Zetacorp          |      108 |     50000.00
     2121 | QMA Assoc.        |      103 |     45000.00
     2114 | Orion Corp        |      102 |     20000.00
     2124 | Peter Brothers    |      107 |     40000.00
     2108 | Holm & Landis     |      109 |     55000.00
     2117 | J.P. Sinclair     |      106 |     35000.00
     2122 | Three-Way Lines   |      105 |     30000.00
     2120 | Rico Enterprises  |      102 |     50000.00
     2106 | Fred Lewis Corp.  |      102 |     65000.00
     2119 | Solomon Inc.      |      109 |     25000.00
     2118 | Midwest Systems   |      108 |     60000.00
     2113 | Ian & Schmidt     |      104 |     20000.00
     2109 | Chen Associates   |      103 |     25000.00
     2105 | AAA Investments   |      101 |     45000.00
     2125 | Z Inc.            |      110 |     20000.00
(22 rows)
```

## Pregunta 4

Creeu una vista anomenada "rep_top_ofi" que llisti totes les dades dels representants de vendes que estan assignats a l'oficina que té més vendes.

```
training=*> CREATE VIEW rep_top_ofi AS
                   SELECT *
                     FROM rep_vendes
                    WHERE oficina_rep IN (SELECT oficina
                                            FROM oficines
                                           WHERE oficina IN (SELECT oficina
                                                               FROM oficines
                                                           GROUP BY oficina, vendes
                                                           ORDER BY vendes DESC
                                                           FETCH FIRST 1 ROW ONLY));
CREATE VIEW

training=*> SELECT *
              FROM rep_top_ofi ;
 num_empl |     nom     | edat | oficina_rep |       carrec        | data_contracte | cap |   quota   |  vendes
----------+-------------+------+-------------+---------------------+----------------+-----+-----------+-----------
      102 | Sue Smith   |   48 |          21 | Representant Vendes | 1986-12-10     | 108 | 350000.00 | 474050.00
      108 | Larry Fitch |   62 |          21 | Dir Vendes          | 1989-10-12     | 106 | 350000.00 | 361865.00
(2 rows)

```

## Pregunta 5

Afegiu un camp a la taula "comandes" anomenat "data_lliurament" que ens permeti emmagatzemar la data d'entrega de la comanda i que per defecte sigui la data d'inserció més 30 dies.

```
training=*> ALTER TABLE comandes
            ADD data_lliurament DATE,
            ALTER data_lliurament SET DEFAULT current_date + 30;
ALTER TABLE

Fem una prova...

training=*> INSERT INTO comandes
            VALUES ((SELECT MAX(num_comanda) + 1
                       FROM comandes),
                     '2022-04-04',
                      2111,
                       103,
                     'aci',
                   '4100x',
                        20,
                      500);
INSERT 0 1

training=*> SELECT * FROM comandes ;
 num_comanda |    data    | clie | rep | fabricant | producte | quantitat |  import  | data_lliurament
-------------+------------+------+-----+-----------+----------+-----------+----------+-----------------
...
      113070 | 2022-04-04 | 2111 | 103 | aci       | 4100x    |        20 |   500.00 | 2022-05-04
(31 rows)
```

## Pregunta 6

Amb una única sentència, modifiqueu l'estructura de la taula "rep_vendes". Ja no ens interessa tenir la informàció del "càrrec" i en canvi volem afegir un camp anomenat "email" per emmagatzemar les adreces de correu electrònic. Cal comprovar que les adreces de correu electrònic contenen el caràcter arroba.

```
training=*> ALTER TABLE rep_vendes
            DROP carrec CASCADE,
            ADD email VARCHAR(50) CHECK (email = '*@*');
ALTER TABLE

training=*> SELECT *
              FROM rep_vendes ;
 num_empl |      nom      | edat | oficina_rep | data_contracte | cap |   quota   |  vendes   | email
----------+---------------+------+-------------+----------------+-----+-----------+-----------+-------
      105 | Bill Adams    |   37 |          13 | 1988-02-12     | 104 | 350000.00 | 367911.00 | 
      109 | Mary Jones    |   31 |          11 | 1989-10-12     | 106 | 300000.00 | 392725.00 | 
      102 | Sue Smith     |   48 |          21 | 1986-12-10     | 108 | 350000.00 | 474050.00 | 
      106 | Sam Clark     |   52 |          11 | 1988-06-14     |     | 275000.00 | 299912.00 | 
      104 | Bob Smith     |   33 |          12 | 1987-05-19     | 106 | 200000.00 | 142594.00 | 
      101 | Dan Roberts   |   45 |          12 | 1986-10-20     | 104 | 300000.00 | 305673.00 | 
      110 | Tom Snyder    |   41 |             | 1990-01-13     | 101 |           |  75985.00 | 
      108 | Larry Fitch   |   62 |          21 | 1989-10-12     | 106 | 350000.00 | 361865.00 | 
      103 | Paul Cruz     |   29 |          12 | 1987-03-01     | 104 | 275000.00 | 286775.00 | 
      107 | Nancy Angelli |   49 |          22 | 1988-11-14     | 108 | 300000.00 | 186042.00 | 
(10 rows)
```

## Pregunta 7

Creeu una taula anomenada "clients_baixa" que tingui la mateixa estructura que la taula "clients" i que contingui els clients que tenen totes les seves comandes anteriors a 1990.

```
training=*> CREATE TABLE clients_baixa AS (
                   SELECT *
                     FROM clients
                    WHERE num_clie IN (SELECT clie
                                         FROM comandes
                                        WHERE date_part('year', data) < '1990')
             );
SELECT 7

training=*> SELECT *
              FROM clients_baixa ;
 num_clie |     empresa      | rep_clie | limit_credit
----------+------------------+----------+--------------
     2111 | JCP Inc.         |      103 |     50000.00
     2102 | First Corp.      |      101 |     65000.00
     2103 | Acme Mfg.        |      105 |     50000.00
     2114 | Orion Corp       |      102 |     20000.00
     2117 | J.P. Sinclair    |      106 |     35000.00
     2106 | Fred Lewis Corp. |      102 |     65000.00
     2118 | Midwest Systems  |      108 |     60000.00
(7 rows)
```
