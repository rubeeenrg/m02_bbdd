# Examen DML, DDL (& Views), CASE

## Pregunta 1

Amb una única sentència SQL, establiu la quota a només 1 (1€) als treballadors
menors de 35 anys i als treballadors de New York i pugeu un 5% a la resta.
Pista: utilitzar CASE

```
UPDATE rep_vendes
   SET quota =
       CASE
       WHEN edat < 35 
         OR oficina_rep IN 
            (SELECT oficina
               FROM oficines
              WHERE ciutat = 'New York')
            THEN 1
       ELSE quota * 1.05
       END;
```

```
 num_empl |      nom      | edat | oficina_rep |       carrec        | data_contracte | cap |   quota   |  vendes   
----------+---------------+------+-------------+---------------------+----------------+-----+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Representant Vendes | 1988-02-12     | 104 | 367500.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Representant Vendes | 1989-10-12     | 106 |      1.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Representant Vendes | 1986-12-10     | 108 | 367500.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Vendes           | 1988-06-14     |     |      1.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Vendes          | 1987-05-19     | 106 |      1.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Representant Vendes | 1986-10-20     | 104 | 315000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Representant Vendes | 1990-01-13     | 101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Vendes          | 1989-10-12     | 106 | 367500.00 | 361865.00
      103 | Paul Cruz     |   29 |          12 | Representant Vendes | 1987-03-01     | 104 |      1.00 | 286775.00
      107 | Nancy Angelli |   49 |          22 | Representant Vendes | 1988-11-14     | 108 | 315000.00 | 186042.00
(10 rows)
```

## Pregunta 2

Esborreu els representants de vendes que tenen unes vendes inferiors a 300000 i
no són directors d'oficina ni caps d'empleats.

```
DELETE FROM rep_vendes
WHERE vendes < 300000
   AND num_empl NOT IN
       (SELECT director
          FROM oficines)
   AND num_empl NOT IN 
       (SELECT cap
          FROM rep_vendes
         WHERE cap IS NOT NULL);
```

```
select * from rep_vendes ;
 num_empl |     nom     | edat | oficina_rep |       carrec        | data_contracte | cap |   quota   |  vendes   
----------+-------------+------+-------------+---------------------+----------------+-----+-----------+-----------
      105 | Bill Adams  |   37 |          13 | Representant Vendes | 1988-02-12     | 104 | 350000.00 | 367911.00
      109 | Mary Jones  |   31 |          11 | Representant Vendes | 1989-10-12     | 106 | 300000.00 | 392725.00
      102 | Sue Smith   |   48 |          21 | Representant Vendes | 1986-12-10     | 108 | 350000.00 | 474050.00
      106 | Sam Clark   |   52 |          11 | VP Vendes           | 1988-06-14     |     | 275000.00 | 299912.00
      104 | Bob Smith   |   33 |          12 | Dir Vendes          | 1987-05-19     | 106 | 200000.00 | 142594.00
      101 | Dan Roberts |   45 |          12 | Representant Vendes | 1986-10-20     | 104 | 300000.00 | 305673.00
      108 | Larry Fitch |   62 |          21 | Dir Vendes          | 1989-10-12     | 106 | 350000.00 | 361865.00
(7 rows)
```

## Pregunta 3

A la base de dades training afegiu un nou client amb:

+ Nom d'empresa "Z Inc.".

+ Ha de tenir el límit de crèdit igual al límit més petit de la resta de
  clients. 

+ El representant de vendes ha de ser aquell representant que tingui menys
clients assignats, en cas d'empat, s'assignarà el representant de vendes amb
menys nombre de vendes (nombre de comandes que ha atès).

+ L'identificador del client ha de ser el següent valor després del valor més
alt de la resta d'identificadors de clients.

+ Suposarem que tots els venedors tenen al menys 1 client assignat, però podria
  haver algun venedor sense vendes.

```
INSERT INTO clients
VALUES ( (SELECT MAX(num_clie)+ 1
            FROM clients),
       'Z Inc.',
       (SELECT rep_clie
          FROM clients
         GROUP BY rep_clie
         ORDER BY COUNT(*), (SELECT COUNT(*)
                               FROM comandes
                              WHERE rep = rep_clie)
         FETCH FIRST 1 ROWS ONLY ),
       (SELECT MIN(limit_credit)
          FROM clients));
```

```
SELECT *
  FROM clients;

 num_clie |      empresa      | rep_clie | limit_credit 
----------+-------------------+----------+--------------
     2111 | JCP Inc.          |      103 |     50000.00
     2102 | First Corp.       |      101 |     65000.00
     2103 | Acme Mfg.         |      105 |     50000.00
     2123 | Carter & Sons     |      102 |     40000.00
     2107 | Ace International |      110 |     35000.00
     2115 | Smithson Corp.    |      101 |     20000.00
     2101 | Jones Mfg.        |      106 |     65000.00
     2112 | Zetacorp          |      108 |     50000.00
     2121 | QMA Assoc.        |      103 |     45000.00
     2114 | Orion Corp        |      102 |     20000.00
     2124 | Peter Brothers    |      107 |     40000.00
     2108 | Holm & Landis     |      109 |     55000.00
     2117 | J.P. Sinclair     |      106 |     35000.00
     2122 | Three-Way Lines   |      105 |     30000.00
     2120 | Rico Enterprises  |      102 |     50000.00
     2106 | Fred Lewis Corp.  |      102 |     65000.00
     2119 | Solomon Inc.      |      109 |     25000.00
     2118 | Midwest Systems   |      108 |     60000.00
     2113 | Ian & Schmidt     |      104 |     20000.00
     2109 | Chen Associates   |      103 |     25000.00
     2105 | AAA Investments   |      101 |     45000.00
     2125 | Z Inc.            |      104 |     20000.00
(22 rows)
```

### EXPLICACIÓ:

Ens pot ajudar primer fer una prèvia:

```
SELECT rep_clie, COUNT(*) AS nombre_clients,
       (SELECT COUNT(*)
          FROM comandes
         WHERE rep = rep_clie) AS nombre_comandes
  FROM clients
 GROUP BY rep_clie
 ORDER BY 2, 3;
```
Que ens mostra

```
 rep_clie | nombre_clients | nombre_comandes 
----------+----------------+-----------------
      104 |              1 |               0
      110 |              1 |               2
      107 |              1 |               3
      109 |              2 |               2
      106 |              2 |               2
      105 |              2 |               5
      108 |              2 |               7
      103 |              3 |               2
      101 |              3 |               3
      102 |              4 |               4
(10 rows)
```

Busquem la 1a fila 

```
SELECT rep_clie, COUNT(*) AS nombre_clients,
       (SELECT COUNT(*)
          FROM comandes
         WHERE rep = rep_clie) AS nombre_comandes
  FROM clients
 GROUP BY rep_clie
 ORDER BY 2, 3
 FETCH FIRST 1 ROWS ONLY;
```

Però només volem el primer camp, com ho podem fer?

Pensant que l'anterior consulta és igual a:

```
SELECT rep_clie, COUNT(*) AS nombre_clients,
       (SELECT COUNT(*)
          FROM comandes
         WHERE rep = rep_clie) AS nombre_comandes
  FROM clients
 GROUP BY rep_clie
 ORDER BY COUNT(*), (SELECT COUNT(*)
                      FROM comandes
                     WHERE rep = rep_clie)
 FETCH FIRST 1 ROWS ONLY;
```

Ara ja només hem de seleccionar la 1a columna:

```
SELECT rep_clie
  FROM clients
 GROUP BY rep_clie
 ORDER BY COUNT(*), (SELECT COUNT(*)
                      FROM comandes
                     WHERE rep = rep_clie)
 FETCH FIRST 1 ROWS ONLY;
```

```
 rep_clie 
----------
      104
(1 row)
```

Un altre possible solució de la consulta complicada:

```
SELECT rep_clie
  FROM clients 
       LEFT JOIN (SELECT COUNT(num_comanda) AS num_comandes, rep
                    FROM comandes
                   GROUP by rep) AS count_comandes
       ON rep_clie = count_comandes.rep
 GROUP BY rep_clie, num_comandes
 ORDER BY COUNT(num_clie), COALESCE(num_comandes,0)
 FETCH FIRST 1 ROWS ONLY;
```

## Pregunta 4

Creeu una vista anomenada "rep_top_ofi" que llisti totes les dades dels
representants de vendes que estan assignats a l'oficina que té més vendes.

```
CREATE VIEW rep_top_ofi 
    AS SELECT *
         FROM rep_vendes
        WHERE oficina_rep =
              (SELECT oficina
                 FROM oficines
                ORDER BY vendes DESC
                FETCH FIRST 1 ROWS ONLY);
```

```
SELECT * FROM rep_top_ofi ;
 num_empl |     nom     | edat | oficina_rep |       carrec        | data_contracte | cap |   quota   |  vendes   
----------+-------------+------+-------------+---------------------+----------------+-----+-----------+-----------
      102 | Sue Smith   |   48 |          21 | Representant Vendes | 1986-12-10     | 108 | 350000.00 | 474050.00
      108 | Larry Fitch |   62 |          21 | Dir Vendes          | 1989-10-12     | 106 | 350000.00 | 361865.00
(2 rows)
```

## Pregunta 5 

Afegiu un camp a la taula "comandes" anomenat "data_lliurament" que ens permeti
emmagatzemar la data d'entrega de la comanda i que per defecte sigui la data
d'inserció més 30 dies.

```
ALTER TABLE comandes
  ADD data_lliurament DATE DEFAULT (CURRENT_DATE + 30);
```

## Pregunta 6

Amb una única sentència, modifiqueu l'estructura de la taula "rep_vendes". Ja
no ens interessa tenir la informàció del "càrrec" i en canvi volem afegir un
camp anomenat "email" per emmagatzemar les adreces de correu electrònic. Cal
comprovar que les adreces de correu electrònic contenen el caràcter arroba.

```
ALTER TABLE rep_vendes 
 DROP COLUMN carrec,
 ADD COLUMN email VARCHAR(50) CHECK (email LIKE '%@%' );
```

```
\d rep_vendes 
                        Table "public.rep_vendes"
     Column     |         Type          | Collation | Nullable | Default 
----------------+-----------------------+-----------+----------+---------
 num_empl       | smallint              |           | not null | 
 nom            | character varying(30) |           | not null | 
 edat           | smallint              |           |          | 
 oficina_rep    | smallint              |           |          | 
 data_contracte | date                  |           | not null | 
 cap            | smallint              |           |          | 
 quota          | numeric(8,2)          |           |          | 
 vendes         | numeric(8,2)          |           |          | 
 email          | character varying(50) |           |          | 
...
```

## Pregunta 7

Creeu una taula anomenada "clients_baixa" que tingui la mateixa estructura que
la taula "clients" i que contingui els clients que tenen totes les seves
comandes anteriors a 1990.

```
CREATE TABLE clients_baixa AS ( 
    SELECT * 
      FROM clients
     WHERE num_clie IN 
           (SELECT clie
              FROM comandes c
             WHERE 1990 > ALL 
                   (SELECT date_part('year', data)
                      FROM comandes 
                     WHERE clie = c.clie))
);
```

O millor encara:

```
CREATE TABLE clients_baixa AS ( 
    SELECT * 
      FROM clients
     WHERE num_clie IN 
           (SELECT clie
              FROM comandes c
             WHERE 1990 >  
                   (SELECT MAX(date_part('year', data))
                      FROM comandes 
                     WHERE clie = c.clie))
);
```
