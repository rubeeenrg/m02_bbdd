# Examen de Transaccions, Accesos, Permisos i Backups

## Exercici 1

Analitzeu les següents sentències especificades en ordre cronològic, tal com
són enviades al sistema gestor de bases de dades. Expliqueu a la taula quins
canvis es realitzen i a on es realitzen. Especifiqueu els valors que
s'obtindran a les instruccions `SELECT`.

```
DELETE FROM punts;

INSERT INTO punts (id, valor)
VALUES (1,10);

BEGIN;

UPDATE punts
   SET valor = 11
 WHERE id = 1;

SAVEPOINT z;

INSERT INTO punts (id, valor)
VALUES (2,20);

SAVEPOINT y;

INSERT INTO punts (id, valor)
VALUES (3,30);

ROLLBACK TO y;

SELECT COUNT(*)
  FROM punts;

ROLLBACK TO z;

COMMIT;

SELECT SUM(valor)
  FROM punts;
```

```
      ┌────────────────────────┬───────────────────────────────────────────────────────────────────────────────────────────────┐
      │     Conn 0             │                                  Comentaris                                                   │
      ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │Delete all              │  S'esborren tots els registres de la taula punts	                                           │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │Afegeix punt (1,10)     │  S'afegeix el punt (1,10)                                                                     │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │Begin Trans.            │                                                                                               │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │Update(1,11)            │  Es modifica el punt (1,11) dintre de la transaccio                                           │
TEMPS ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │Savepoint z             │                                                                                               │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │Afegeix (2,20)          │  S'afegeix el punt(2,20) dintre de la transaccio i despres del punt de control z              │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │Savepoint y             │                                                                                               │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │Afegeix (3,30)          │  S'afegeix el punt(3,30) dintre de la transaccio i despres del punt de control y              │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │Rollback to y           │  Perdem el punt (3,30)                                                                        │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │SELECT COUNT(*) ->      │  2: (1,11),(2,20) pero fora de la Connexio 0 nomes 1:(1,10)                                   │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │ROLLBACK to z           │  Perdem el punt (2,20)                                                                        │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  │   │COMMIT                  │  Es modifica realment el punt (1,11)                                                          │
  │   ├────────────────────────┼───────────────────────────────────────────────────────────────────────────────────────────────┤
  ▼   │SELECT SUM(valor) ->    │  11                                                                                           │
      └────────────────────────┴───────────────────────────────────────────────────────────────────────────────────────────────┘
```

## Exercici 2

Analitzeu les següents sentències especificades en ordre cronològic, tal com són enviades al sistema gestor de bases de dades. Expliqueu quins canvis es realitzen i a on es realitzen. Especifiqueu els valors que s'obtindran amb les sentències `SELECT`. Cal tenir en compte que cada sentència s'executa en una connexió determinada.

```
DELETE FROM punts;            -- Connexió 0
INSERT INTO punts (id, valor)
VALUES (10,100);              -- Connexió 0

INSERT INTO punts (id, valor)
VALUES (20,200);              -- Connexió 0

BEGIN;                        -- Connexió 1

UPDATE punts
   SET valor = 201
 WHERE id = 20;               -- Connexió 1

SAVEPOINT a;                  -- Connexió 1

UPDATE punts 
   SET valor = 101
 WHERE id = 10;               -- Connexió 1

BEGIN;                        -- Connexió 2
SELECT valor 
  FROM punts
 WHERE id = 10;               -- Connexió 2

UPDATE punts
   SET valor = 102
 WHERE id = 10;               -- Connexió 2

ROLLBACK TO a;                -- Connexió 1

UPDATE punts
   SET valor = 202
 WHERE id = 20;               -- Connexió 2

COMMIT;                       -- Connexió 2

SELECT valor
  FROM punts
 WHERE id = 20;               -- Connexió 1

COMMIT;                       -- Connexió 1

SELECT valor
  FROM punts
 WHERE id = 20;               -- Connexió 0
```

```
        ┌─────────────────────┬───────────────┬───────────────┬──────────────────────────────────────────────────────────────────┐
        │      Conn 0         │   Conn 1      │     Conn 2    │                          Comentaris                              │
        ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │Delete all           │               │               │                                                                  │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │Afegit punt(10,100)  │               │               │                                                                  │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │Afegit punt(20,200)  │               │               │ Afegits definitivament punts (10,100) i (20,200)                 │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │Begin Transac. │               │                                                                  │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │Update (20,201)│               │ Es modifica en transaccio (20,201)                               │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │Savepoint a    │               │                                                                  │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │Update (10,101)│               │ Es modifica en transaccio (10,101)                               │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │               │Begin Transac. │                                                                  │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
  TEMPS │                     │               │Select -> 100  │                                                                  │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │               │Update (10,102)│ Queda bloquejat esperant que acabi la trans. de Conn 1           │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │Rollback to a  │               │ Es perden els canvis de (10,101) i es desbloqueja la Conn 2      │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │               │Update (20,202)│ Queda bloquejat esperant que acabi la trans. de Conn 1           │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │               │Commit         │ Encara no s'executa pel bloqueig                                 │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │SELECT -> 201  │               │                                                                  │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    │   │                     │COMMIT         │               │ Es fa aquest COMMIT -> (20,201) i despres el commit de Conn 2 -> │
    │   │                     │               │               │ (20,202)                                                         │
    │   ├─────────────────────┼───────────────┼───────────────┼──────────────────────────────────────────────────────────────────┤
    ▼   │SELECT -> 202        │               │               │                                                                  │
        └─────────────────────┴───────────────┴───────────────┴──────────────────────────────────────────────────────────────────┘
```

## Exercici 3

Escull una opció de les següents preguntes:

+ Pregunta 1

	Habitualment, una còpia de seguretat lògica:

	1. fa una còpia de tots els fitxers que defineixen la base de dades en un únic fitxer
    2. **conté còpies d'informació d'una base de dades com poden ser taules o esquemes.**
    3. es recomanable fer-la en escenaris de desastres totals per tornar al passat.	
    4. és equivalent a còpia en calent.
       
+ Pregunta 2

	Habitualment, una còpia de seguretat física:
    
	1. **és la recomanada per gestionar SGBD de mida gran.**
    2. conté còpies d'informació d'una base de dades com poden ser taules o esquemes.
    3. no hi ha cap informació relacionada amb l’entorn de la base de dades.	
    4. és la recomanada quan volem fer una actualització de versió de SGBD.
       
+ Pregunta 3

	Quina de les següents ordres s’utilitza per fer un backup lògic de totes
les bases de dades d’un SGBD postgresql? 
    
	1. **pg_dumpall**
    2. pg_dump
    3. pg_dum -all
    4. borg	
       
+ Pregunta 4

	1. Una còpia de seguretat en calent és aquella que es fa quan no hi ha cap fitxer de la base de dades obert.
	2. Un cold backup és el recomanable per a serveis de 24h.  
	3. **Un cold backup garanteix la consistència de les dades.**
	4. A PostgreSQL no cal parar el servei quan es fa una còpia en fred.
   
+ Pregunta 5

	Volem restaurar la taula productes d’un backup lògic (training.tar) que
s’ha fet amb l’eina habitual de postgresql d’una base de dades anomenada
training. Quina de les següents respostes et sembla més adient?

	1. **pg_restore -d training -t productes training.tar**
    2. pg_dump -d training -t productes training.tar 
    3. psql -d training -t productes < training.tar
    4. No es pot fer. Però sí restaurar tota la base de dades i eliminar la resta.
