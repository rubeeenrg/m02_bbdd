# Examen Accés permisos (DCL)

## Exercici 1

Expliqueu les modificacions que s'han de fer en un sistema gestor de bases de dades postgresql per tal que:

* Els usuaris no especificats no s'han de poder connectar a cap base de dades des de cap adreça IPv4.

Revisem que al fitxer **/etc/postgresql/13/main/postgresql.conf** tenim les següents línes descomentades i amb aquests valors:

```
localhost='*'
port=5432
```

Ens fiquem al fitxer **/etc/postgresql/13/main/pg_hba.conf** I AL FINAL DEL TOT FIQUEM...

```
host    all     all   0.0.0.0/0     reject
```

Reiniciem el servei:

```
isx48062351@j28:~$ sudo systemctl restart postgresql
```

* Existeixi un usuari anomenat "a10" amb contrasenya "10" guardada al sistema gestor de bases.

```
training=*# CREATE USER a10 WITH PASSWORD '10';
CREATE ROLE
```

* Existeixi un altre usuari anomenat "b20" amb contrasenya "20" guardada al sistema gestor de bases de dades.

* L'usuari "a10" ha de poder accedir a qualsevol base de dades usant la contrasenya des de qualsevol adreça IP de la xarxa 10.1.0.0/16.

Ens fiquem al fitxer **/etc/postgresql/13/main/pg_hba.conf**...

```
host    all     a10   10.1.0.0/16     md5
```

* L'usuari "a10" ha de poder accedir a la base de dades amb el mateix nom que l'usuari sense usar contrasenya des de l'adreça IP 10.1.2.3.

```
host    a10     a10   10.1.2.3/32     trust
```

* L'usuari "a10" no ha de poder accedir des de qualsevol altre lloc que no siguin els anteriorment especificats.

```
host    all     a10   0.0.0.0/0     reject
```

* L'usuari "b20" no ha de poder accedir a la base de dades "b" des de les adreces IP de la xarxa 10.1.2.0/24.

```
training=*# CREATE USER b20 WITH PASSWORD '20';
CREATE ROLE
```

```
host    b     b20   10.1.2.0/24     reject
```

* L'usuari "b20" ha de poder accedir a qualsevol base de dades des de qualsevol altre lloc amb contrasenya.

```
host    all     b20   0.0.0.0/0     md5
```

* L'usuari "b20" no ha de poder accedir a partir del 6 d'abril de 2022.

```
training=*# ALTER ROLE b20 VALID UNTIL '2022-04-06';
ALTER ROLE

training=*# \du
                                            List of roles
    Role name  |                         Attributes                         | Member of
-------------+------------------------------------------------------------+------------
    a10         |                                                            | {}
    b20         | Password valid until 2022-04-06 00:00:00+02                | {}
    isx48062351 | Cannot login                                               | {postgres}
    postgres    | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
```

FINALEMNT L'ARXIU **/etc/postgresql/13/main/pg_hba.conf** quedarà de la següent manera:

```
host    a10     a10   10.1.2.3/32   trust
host    all     a10   10.1.0.0/16   md5
host    all     a10   0.0.0.0/0     reject
host    b       b20   10.1.2.0/24   reject
host    all     b20   0.0.0.0/0     md5
host    all     all   0.0.0.0/0     reject
```

* Expliqueu també com comprovaríeu el correcte funcionament.

```
isx48062351@j28:~$ ip a
...
2: enp5s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 40:8d:5c:e4:37:00 brd ff:ff:ff:ff:ff:ff
    inet 10.200.244.228/24 brd 10.200.244.255 scope global dynamic noprefixroute enp5s0
        valid_lft 17809sec preferred_lft 17809sec
    inet6 fe80::428d:5cff:fee4:3700/64 scope link noprefixroute
        valid_lft forever preferred_lft forever
....
```

**Des qualsevol IP dins de la xarxa 10.1.0.0/16:**

```
psql -h 10.200.244.228 -U a10 training --> Sí pot
psql -h 10.200.244.228 -U user1 training --> No pot
```

**Des la IP 10.1.2.3:**

```
psql -h 10.200.244.228 -U a10 a10 --> Sí pot
psql -h 10.200.244.228 -U a10 training --> Sí pot
psql -h 10.200.244.228 -U user1 training --> No pot
```

**Des la IP 192.168.200.30:**

```
psql -h 10.200.244.228 -U a10 a10 --> No pot
psql -h 10.200.244.228 -U a10 training --> No pot
```

**Des qualsevol IP dins de la xarxa 10.1.2.0/24:**

```
psql -h 10.200.244.228 -U b20 b--> No pot
psql -h 10.200.244.228 -U b20 training --> Sí pot
psql -h 10.200.244.228 -U b20 training --> Sí pot
```

**Des la IP 192.168.200.30:**

```
psql -h 10.200.244.228 -U user1 a10 --> No pot
psql -h 10.200.244.228 -U user1 training --> No pot
psql -h 10.200.244.228 -U user1 b --> No pot
```

## Exercici 2

Suposant que tenim una base de dades amb 3 taules anomenades "proveidors", "productes" i "clients" propietat de l'usuari "distribuidor". Quines sentències s'han d'executar per implementar el següent escenari:

* Un usuari anomenat "admin" amb poders absoluts.

```
training=*# CREATE USER admin SUPERUSER PASSWORD 'jupiter';
```

* Un usuari anomenat "gestor" que ha de poder modificar les dades i l'estructura de la base de dades, només ha de poder realitzar 2 connexions concurrents.

```
training=*# CREATE USER gestor CONNECTION LIMIT 2 PASSWORD 'gestor';
training=*# GRANT UPDATE IN ALL TABLES ON SCHEMA public TO gestor;
```

* Un usuari anomenat "lectura" que només ha de poder llegir les dades de la base de dades.

```
training=*# CREATE USER lectura WITH PASSWORD 'lectura';
training=*# GRANT SELECT IN ALL TABLES ON SCHEMA public TO gestor;
```

* Un usuari anomenat "compres" que ha de poder consultar tota la base de dades però només ha de poder modificar (afegir, editar i esborrar) les dades de les taules "proveidors" i "productes".

```
training=*# CREATE USER compres WITH PASSWORD 'compres';
training=*# GRANT SELECT IN ALL TABLES ON SCHEMA public TO compres;
training=*# GRANT INSERT,UPDATE,DELETE ON proveidors,productes TO compres;
--------------------------------------------------------------------------
training=* ALTER TABLE proveidors OWNER TO compres; --> PODEM FER AIXÒ TAMBÉ!!!!! (EN COMPTES DE LA D'ADALT)
training=* ALTER TABLE productes OWNER TO compres;
```

* Un usuari anomenat "vendes" que ha de poder consultar i modificar (afegir, editar i esborrar) les dades de la taula "clients", només ha de poder modificar (editar) la columna "estoc" de la taula "productes" especificant quin producte a partir de la columna "id_prod" i no ha de poder veure les dades de la taula "proveidors" (assegurar-se que se li treu aquest privilegi).

```
training=*# CREATE USER vendes WITH PASSWORD 'vendes';
training=*# GRANT INSERT,UPDATE,DELETE ON clients TO vendes;
------------------------------------------------------------
training=* ALTER TABLE clients OWNER TO vendes; --> PODEM FER AIXÒ TAMBÉ!!!!! (EN COMPTES DE LA D'ADALT)
training=*# GRANT UPDATE(estoc), SELECT(id_prod) ON productes TO vendes;
training=*# REVOKE SELECT ON proveidors FROM vendes;
```

* Expliqueu també com comprovaríeu el correcte funcionament.

```
psql -h 10.200.244.228 -U admin bbdd_ex2

training=*# SELECT ... FROM [proveidors/productes/clients] --> OK!

training=*# INSERT INTO [proveidors/productes/clients] ... --> OK!

training=*# UPDATE [proveidors/productes/clients] ... --> OK!

training=*# DELETE FROM [proveidors/productes/clients] ... --> OK!
```

```
psql -h 10.200.244.228 -U gestor bbdd_ex2   --> CONN 1 (OK!)

psql -h 10.200.244.228 -U gestor bbdd_ex2   --> CONN 2 (OK!)

psql -h 10.200.244.228 -U gestor bbdd_ex2   --> CONN 3 (ERROR!)

training=*# SELECT ... FROM [proveidors/productes/clients] --> ERROR!

training=*# INSERT INTO [proveidors/productes/clients] ... --> ERROR!

training=*# UPDATE [proveidors/productes/clients] ... --> OK!

training=*# DELETE FROM [proveidors/productes/clients] ... --> ERROR!
```

```
psql -h 10.200.244.228 -U lectura bbdd_ex2

training=*# SELECT ... FROM [proveidors/productes/clients] --> OK!

training=*# INSERT INTO [proveidors/productes/clients] ... --> ERROR!

training=*# UPDATE [proveidors/productes/clients] ... --> ERROR!

training=*# DELETE FROM [proveidors/productes/clients] ... --> ERROR!
```

```
psql -h 10.200.244.228 -U compres bbdd_ex2

training=*# SELECT ... FROM [proveidors/productes/clients] --> OK!

training=*# INSERT INTO [proveidors/productes] ... --> OK! (EN LA TAULA CLIENTS = ERROR!)

training=*# UPDATE [proveidors/productes] ... --> OK! (EN LA TAULA CLIENTS = ERROR!)

training=*# DELETE FROM [proveidors/productes] ... --> OK! (EN LA TAULA CLIENTS = ERROR!)
```

```
psql -h 10.200.244.228 -U compres bbdd_ex2

training=*# SELECT ... FROM [productes/clients] --> OK! (EN LA TAULA PROVEIDORS = ERROR!)

training=*# INSERT INTO [clients] ... --> OK! (EN LA TAULA PROVEIDORS I PRODUCTES = ERROR!)

training=*# UPDATE [clients] ... --> OK! (EN LA TAULA PROVEIDORS = ERROR!) (A PRODUCTES NOMÉS PODEM ACTUALITZAR EL CAMP ESTOC O UTILITZAN LA COLUMNA "id_prod" --> té permisos de SELECT!)

training=*# DELETE FROM [clients] ... --> OK! (EN LA TAULA PROVEIDORS I PRODUCTES = ERROR!)
```
