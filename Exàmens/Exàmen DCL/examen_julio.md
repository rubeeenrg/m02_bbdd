# Examen Transaccions, Accés, Permisos, Backups

## Exercici 1
 

Expliqueu les modificacions que s'han de fer en un sistema gestor de bases de dades postgresql per tal que:

+ Els usuaris no especificats no s'han de poder connectar a cap base de dades
  des de cap adreça IPv4.

No cal posar res per fer complir aquesta directiva. Tot i que si hi ha paranoia
es podria posar un reject però AL FINAL de les directives de tipus
host(TCP/IP), si no la liem!

Les directives les posem al fitxer configuració d'autenticació
`/etc/postgresql/13/main/pg_hba.conf`

+ Existeixi un usuari anomenat "a10" amb contrasenya "10" guardada al sistema
  gestor de bases.

```
CREATE USER a10 PASSWORD '10';
```

+ Existeixi un altre usuari anomenat "b20" amb contrasenya "20" guardada al
  sistema gestor de bases de dades.

```
CREATE USER b20 PASSWORD '20'
```

+ L'usuari "a10" ha de poder accedir a qualsevol base de dades usant la
  contrasenya des de qualsevol adreça IP de la xarxa 10.1.0.0/16.

Afegim la directiva:

```
host    all        a10             10.1.0.0/16           md5
```

+ L'usuari "a10" ha de poder accedir a la base de dades amb el mateix nom que
  l'usuari sense usar contrasenya des de l'adreça IP 10.1.2.3.

Afegim la directiva que ha d'estar :

```
host    a10        a10             10.1.2.3/32           trust
```

+ L'usuari "a10" no ha de poder accedir des de qualsevol altre lloc que no
  siguin els anteriorment especificats.

no cal posar res


+ L'usuari "b20" no ha de poder accedir a la base de dades "b" des de les
  adreces IP de la xarxa 10.1.2.0/24.

Afegim la directiva:

```
host    b        b20             10.1.2.0/24           reject
```

+ L'usuari "b20" ha de poder accedir a qualsevol base de dades des de qualsevol
  altre lloc amb contrasenya.

Afegim la directiva:

```
host    all        b20             0.0.0.0/0           md5
```

+ L'usuari "b20" no ha de poder accedir a partir del 6 d'abril de 2022.

Podríem haver creat al principi l'usuari així:

```
CREATE USER b20 PASSWORD '20' VALID UNTIL '2022-04-06';
```

o modificar-lo:

```
ALTER USER B20 VALID UNTIL '2022-04-06'
```

Recordeu que CREATE USER X és un alias a CREATE ROLE X LOGIN

+ Expliqueu també com comprovaríeu el correcte funcionament.

Com sempre perquè funcionés hauriem de fer:

```
psql -U a10  -h  .....
```

...

**Solució:**

Com sempre, al fitxer `/var/lib/pgsql/data/postgresql.conf` posem:

```
listen_addresses='*'
```

Mentre que a `/var/lib/pgsql/data/pg_hba.conf` les directives serien:

```
#TYPE   DATABASE   USER            ADDRESS               METHOD
host    a10        a10             10.1.2.3/32           trust
host    all        a10             10.1.0.0/16           md5
host    b          b20             10.1.2.0/24           reject
host    all        b20             0.0.0.0/0             md5
```

Com sempre perquè funcionés hauriem de fer:

```
# systemctl restart postgresql
``` 

Com a usuari postgres fariem:

```
CREATE USER a10 PASSWORD '10';
CREATE USER b20 PASSWORD '20' VALID UNTIL '2022-04-06';
```

## Exercici 2

Suposant que tenim una base de dades amb 3 taules anomenades "proveidors", "productes" i "clients" propietat de l'usuari "distribuidor". Quines sentències s'han d'executar per implementar el següent escenari:

+ Un usuari anomenat "admin" amb poders absoluts.
+ Un usuari anomenat "gestor" que ha de poder modificar les dades i l'estructura de la base de dades, només ha de poder realitzar 2 connexions concurrents.
+ Un usuari anomenat "lectura" que només ha de poder llegir les dades de la base de dades.
+ Un usuari anomenat "compres" que ha de poder consultar tota la base de dades però només ha de poder modificar (afegir, editar i esborrar) les dades de les taules "proveidors" i "productes".
+ Un usuari anomenat "vendes" que ha de poder consultar i modificar (afegir, editar i esborrar) les dades de la taula "clients", només ha de poder modificar (editar) la columna "estoc" de la taula "productes" especificant quin producte a partir de la columna "id_prod" i no ha de poder veure les dades de la taula "proveidors" (assegurar-se que se li treu aquest privilegi).

Aquí a la 2a part diu *només ha de poder modificar (editar) la columna "estoc" de la taula "productes" especificant quin producte a partir de la columna "id_prod"*

Imaginem que volem fer un update de la columna estoc a partir de columna id_prod

UPDATE

SET -> permís d'UPDATE

WHERE -> permís de SELECT 

+ Expliqueu també com comprovaríeu el correcte funcionament.

**Solució**

```
CREATE USER admin PASSWORD 'a' SUPERUSER;
CREATE USER gestor PASSWORD 'g' CONNECTION LIMIT 2;
CREATE USER lectura PASSWORD 'l';
CREATE USER compres PASSWORD 'c';
CREATE USER vendes PASSWORD 'v';
GRANT distribuidor TO gestor;
GRANT SELECT ON ALL TABLES IN SCHEMA PUBLIC TO lectura, compres;
GRANT INSERT, UPDATE, DELETE ON proveidors, productes TO compres;
GRANT SELECT, INSERT, UPDATE, DELETE ON clients TO vendes;
GRANT UPDATE(estoc), SELECT(id_prod) ON productes TO vendes;
REVOKE SELECT ON proveidors FROM vendes;
```
