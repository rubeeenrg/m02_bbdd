# PRIMERA PART: DCL. DATA CONTROL LANGUAGE.

**TENIM EL SEGÜENT:**

Una base de dades d’una llibreria. Amb dues taules LLIBRES i PROVEIDORS. (NO CAL CREAR-LES!)

EXPLICA quina configuració ha de tenir Postgres i quines sentències s’han d’executar per obtenir el següent escenari. Utilitzeu noms que siguin descriptius.

**FITXERS QUE UTILITZAREM:**

```
/etc/postgresql/13/main/pg_hba.conf
/var/lib/pgsql/data/postgresql.conf
```

Els usuaris guardaran les seves contrasenyes al postgresql.

**Dins del fitxer postgresql.conf:**

```
listen_addresses='*'
```

Reiniciem:

```
$ systemctl restart postgresql
```

Un usuari administrador de la base de dades:

Ha de poder veure i modificar totes les taules. Ha de poder crear nous usuaris i assignar els permisos corresponents.

```
llibreria=# CREATE ROLE administrador WITH OPTION superuser createrole;
lliberia=# GRANT SELECT, UPDATE, INSERT, DELETE ON llibres,proveidors TO administrador;
```

**(AQUESTES DUES LÍNES RESPONEN ALS 2 ENUNCIATS)**

Només pot treballar en local i es connectarà amb l’usuari de sistema.

**Dins del fitxer pg_hba.conf:**

```
host    llibreria     administrador        10.200.243.224/32     ident
host    llibreria     all                         0.0.0.0/0      reject
```

Reiniciem:

```
$ systemctl restart postgresql
```

Un grup d’usuaris de manteniment (que tindrà dos usuaris, els dos llibreters)

Han de poder veure i modificar totes les taules

```
llibreria=# CREATE ROLE manteniment;
llibreria=# CREATE USER manteniment1 WITH PASSWORD 'manteniment1';
llibreria=# CREATE USER manteniment2 WITH PASSWORD 'manteniment2';
llibreria=# GRANT manteniment TO manteniment1,manteniment2;
llibreria=# GRANT SELECT, UPDATE, INSERT, DELETE ON llibres,proveidors TO manteniment;
```

S’han de poder connectar des de qualsevol ordinador de la llibreria (192.168.30.*)

**Dins del fitxer pg_hba.conf:**

```
host    llibreria     +manteniment        192.168.30.0/24       md5
host    llibreria     all                      0.0.0.0/0        reject
```

Reiniciem:

```
$ systemctl restart postgresql
```

Un grup d’usuaris de consulta (de moment tindrà 2 usuaris)

Només poden consultar la taula de llibres

```
llibreria=# CREATE ROLE consulta;
llibreria=# CREATE USER user1 WITH PASSWORD 'user1';
llibreria=# CREATE USER user2 WITH PASSWORD 'user2';
llibreria=# GRANT consulta TO user1,user2;
llibreria=# GRANT SELECT ON llibres TO manteniment;
```

S’han de poder connectar des de qualsevol ordinador de la llibreria (192.168.30.*). No cal que demani password

**Dins del fitxer pg_hba.conf:**

```
host    llibreria     +consulta           192.168.30.0/24       trust
host    llibreria     all                      0.0.0.0/0        reject
```
Reiniciem:

```
$ systemctl restart postgresql
```

L'usuari de l'aplicació web:

Només pot consultar la taula de llibres

```
llibreria=# CREATE USER web WITH PASSWORD 'web';
llibreria=# GRANT SELECT ON llibres TO web;
```

Només s'ha de poder connectar des de l'ordinador que té el servidor web. (192.168.30.21).

**Dins del fitxer pg_hba.conf:**

```
host    llibreria     web        192.168.30.21/32     md5
host    llibreria     all              0.0.0.0/0      reject
```

Reiniciem:

```
$ systemctl restart postgresql
```

Escriu també les sentències que caldria executar per comprovar el correcte funcionament i si donen error o no.

-- COMPROVACIONS CONNEXIONS

* Administrador

psql -h <ip-server> -U administrador llibreria -- OK des de 10.200.243.224. ERROR altrament

* Manteniment

psql -h <ip_server> -U manteniment1 llibreria -- OK des de 192.168.30.*. Demana password. ERROR altrament

* Metges

psql -h <ip_server> -U user1 llibreria -- OK des de 192.168.30.*. NO demanarà password. ERROR altrament

* Servidor Web

psql -h <ip-server> -U web llibreria -- OK des de 192.168.30.21. Demana password. ERROR altrament

-- COMPROVACIONS SQL

* usuari administrador

SELECT * FROM llibres; -- Ok

SELECT * FROM proveidors; -- Ok

INSERT llibres...  -- Ok

INSERT proveidors...  -- Ok

DELETE FROM llibres...	-- Ok

DELETE FROM proveidors...	-- Ok

UPDATE llibres...	-- Ok

UPDATE proveidors...	-- Ok

INSERT INTO llibres...	-- Ok

INSERT INTO proveidors...	-- Ok


* Manteniment

SELECT * FROM llibres; -- Ok

SELECT * FROM proveidors; -- Ok

INSERT llibres...  -- Ok

INSERT proveidors...  -- Ok

DELETE FROM llibres...	-- Ok

DELETE FROM proveidors...	-- Ok

UPDATE llibres...	-- Ok

UPDATE proveidors...	-- Ok

INSERT INTO llibres...	-- Ok

INSERT INTO proveidors...	-- Ok


-- Consulta

* Desde qualsevol ip

psql -h <ip_server> -U user1 llibreria -- Ok

psql -h <ip_server> -U user2 llibreria -- Ok

SELECT * FROM llibres; -- Ok

SELECT * FROM proveidors; -- ERROR

INSERT llibres...  -- ERROR

INSERT proveidors...  -- ERROR

DELETE FROM llibres...	-- ERROR

DELETE FROM proveidors...	-- ERROR

UPDATE llibres...	-- ERROR

UPDATE proveidors...	-- ERROR

INSERT INTO llibres...	-- ERROR

INSERT INTO proveidors...	-- ERROR


-- Usuari aplicació web

* Desde la ip que hem posat (192.168.30.21)

psql -h <ip_server> -U web llibreria -- Ok

SELECT * FROM llibres; -- Ok

SELECT * FROM proveidors; -- ERROR

INSERT llibres...  -- ERROR

INSERT proveidors...  -- ERROR

DELETE FROM llibres...	-- ERROR

DELETE FROM proveidors...	-- ERROR

UPDATE llibres...	-- ERROR

UPDATE proveidors...	-- ERROR

INSERT INTO llibres...	-- ERROR

INSERT INTO proveidors...	-- ERROR


Finalment volem que els usuaris de manteniment (que has creat abans) també puguin crear usuaris. Escriu la/les comanda/es necessària/es.

```
training=# ALTER ROLE manteniment WITH createrole;
```
