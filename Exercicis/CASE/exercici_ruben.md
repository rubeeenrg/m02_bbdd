**REMEMBER:** SI NO FEM CAMP DISCRIMINATORI (tipus) L'UNION NO SERVEIX DE RES

```
CREATE OR REPLACE VIEW grup_n2j
    AS SELECT nom, telefon, 'profe' AS tipus
             FROM profes
          WHERE grup = 'n2j';
          UNION
         SELECT nom, telefon, 'alumne' AS tipus
            FROM alumnes
         WHERE grup = 'n2j';
```

```
training=# SELECT nom, carrec,
CASE
WHEN oficina_rep = 13 AND carrec = 'Representant Vendes' THEN 'Tipus A'
WHEN oficina_rep = 11 AND carrec <> 'Representant Vendes' THEN 'Tipus B'
ELSE
'Tipus C'
END AS Tipus
FROM rep_vendes;
      nom      |       carrec        |  tipus  
---------------+---------------------+---------
 Bill Adams    | Representant Vendes | Tipus A
 Mary Jones    | Representant Vendes | Tipus C
 Sue Smith     | Representant Vendes | Tipus C
 Sam Clark     | VP Vendes           | Tipus B
 Bob Smith     | Dir Vendes          | Tipus C
 Dan Roberts   | Representant Vendes | Tipus C
 Tom Snyder    | Representant Vendes | Tipus C
 Larry Fitch   | Dir Vendes          | Tipus C
 Paul Cruz     | Representant Vendes | Tipus C
 Nancy Angelli | Representant Vendes | Tipus C
(10 rows)
```

* Mostrar per a cada producte totes les sevs dades, i a la dreta un camp titulat "Alerta existències" on, si la suma de les quantitats de dues últimes comandes (en direm N) supera les existències actuals, hi surti el missatge "ALERTA: últimes comandes sumen N")(on N és el número que hem anomenat abans). Si no, que no hi surti res a la columna

-- Per exemple:

```
 id_fab | id_producto |    descripcion    | precio  | existencias |         Alerta existències          
--------+-------------+-------------------+---------+-------------+-------------------------------------
...
 aci    | 41001       | Articulo Tipo 1   |   55.00 |         277 |
 imm    | 775c        | Riostra 1-Tm      | 1425.00 |           5 | ALERTA: últimes 2 comandes sumen 22
 aci    | 4100z       | Montador          | 2500.00 |          28 |
...
```

```
training2=# SELECT *,
            CASE WHEN (SELECT SUM(comandes.quantitat)
                         FROM comandes
                        WHERE productes.id_fabricant = comandes.fabricant
                          AND productes.id_producte = comandes.producte
                          AND comandes.num_comanda IN (SELECT c.num_comanda
                                                         FROM comandes c
                                                        WHERE c.fabricant = productes.id_fabricant
                                                          AND c.producte = productes.id_producte
                                                        ORDER BY c.data DESC LIMIT 2)) > productes.estoc
                  THEN 'ALERTA: últimes 2 comandes sumen ' || (SELECT SUM(comandes.quantitat)
                                                                 FROM comandes
                                                                WHERE productes.id_fabricant = comandes.fabricant
                                                                  AND productes.id_producte = comandes.producte
                                                                  AND comandes.num_comanda IN (SELECT c.num_comanda
                                                                                                 FROM comandes c
                                                                                                WHERE productes.id_fabricant = c.fabricant  
                                                                                                  AND productes.id_producte = c.producte
                                                                                                ORDER BY c.data DESC LIMIT 2))
                  ELSE NULL
              END AS "Alerta existències"
              FROM productes;

 id_fabricant | id_producte |     descripcio     |  preu   | estoc |         Alerta existències          
--------------+-------------+--------------------+---------+-------+-------------------------------------
 rei          | 2a45c       | V Stago Trinquet   |   79.00 |   210 |
 aci          | 4100y       | Extractor          | 2750.00 |    25 |
 qsa          | xk47        | Reductor           |  355.00 |    38 |
 bic          | 41672       | Plate              |  180.00 |     0 |
 imm          | 779c        | Riosta 2-Tm        | 1875.00 |     9 |
 aci          | 41003       | Article Tipus 3    |  107.00 |   207 |
 aci          | 41004       | Article Tipus 4    |  117.00 |   139 |
 bic          | 41003       | Manovella          |  652.00 |     3 |
 imm          | 887p        | Pern Riosta        |  250.00 |    24 |
 qsa          | xk48        | Reductor           |  134.00 |   203 |
 rei          | 2a44l       | Frontissa Esq.     | 4500.00 |    12 |
 fea          | 112         | Coberta            |  148.00 |   115 |
 imm          | 887h        | Suport Riosta      |   54.00 |   223 |
 bic          | 41089       | Retn               |  225.00 |    78 |
 aci          | 41001       | Article Tipus 1    |   55.00 |   277 |
 imm          | 775c        | Riosta 1-Tm        | 1425.00 |     5 | ALERTA: últimes 2 comandes sumen 22
 aci          | 4100z       | Muntador           | 2500.00 |    28 |
 qsa          | xk48a       | Reductor           |  117.00 |    37 |
 aci          | 41002       | Article Tipus 2    |   76.00 |   167 |
 rei          | 2a44r       | Frontissa Dta.     | 4500.00 |    12 | ALERTA: últimes 2 comandes sumen 15
 imm          | 773c        | Riosta 1/2-Tm      |  975.00 |    28 |
 aci          | 4100x       | Peu de rei         |   25.00 |    37 |
 fea          | 114         | Bancada Motor      |  243.00 |    15 | ALERTA: últimes 2 comandes sumen 16
 imm          | 887x        | Retenidor Riosta   |  475.00 |    32 |
 rei          | 2a44g       | Passador Frontissa |  350.00 |    14 |
(25 rows)
```
