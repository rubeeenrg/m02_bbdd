# Backup lògic

## Eines de PostgreSQL

Quan el sistema gestor de Base de Dades Relacional (SGBDR o RDBMS en anglès) és
PostgreSQL, les ordres clàssiques per fer la còpia de base de dades són:

+ `pg_dump db_name > file_name.sql / pg_dumb -Fc db_name > file_name.dump-` per copiar una base de dades del SGBDR a un fitxer script o de
  tipus arxiu

+ `pg_dumpall > file_name.sql` per copiar totes les bases de dades del SGBDR a un fitxer script
  SQL

Anàlogament, quan volem restaurar una base de dades si el fitxer destí ha estat un script SQL farem la restauració amb l'ordre `psql -U username -d dbname < filename.sql`, mentre que si el fitxer destí és de tipus binari l'ordre a utilitzar serà `pg_restore -U username -Fc -d dbname < filename.dump`.

Anem a jugar dintre del nostre SGBDR. Respon i després *executa les
instruccions comprovant empíricament que són correctes*.

## Pregunta 1.

Quina instrucció ens fa una còpia de seguretat lògica de la base de dades training (de tipus script SQL) ?

```
isx48062351@j28:~/exercicis_backups$ pg_dump --dbname=training --username=isx48062351 --format=plain --file=training_bkp.sql

isx48062351@j28:~/exercicis_backups$ ls
training_bkp.sql
```

## Pregunta 2.

El mateix d'abans però ara el destí és un directori.

```
isx48062351@j28:~/exercicis_backups$ pg_dump -F d training -f backups/

isx48062351@j28:~/exercicis_backups$ ls -la backups/
total 40
drwxr-xr-x 2 isx48062351 hisx2 4096 Mar 25 08:18 .
drwxr-xr-x 3 isx48062351 hisx2 4096 Mar 25 08:18 ..
-rw-r--r-- 1 isx48062351 hisx2  426 Mar 25 08:18 3017.dat.gz
-rw-r--r-- 1 isx48062351 hisx2  156 Mar 25 08:18 3018.dat.gz
-rw-r--r-- 1 isx48062351 hisx2  362 Mar 25 08:18 3019.dat.gz
-rw-r--r-- 1 isx48062351 hisx2  359 Mar 25 08:18 3020.dat.gz
-rw-r--r-- 1 isx48062351 hisx2  517 Mar 25 08:18 3021.dat.gz
-rw-r--r-- 1 isx48062351 hisx2 8266 Mar 25 08:18 toc.dat
```

## Pregunta 3.

El mateix d'abans però ara el destí és un fitxer tar.

```
isx48062351@j28:~/exercicis_backups$ pg_dump -F t training > training_bkp2.tar

isx48062351@j28:~/exercicis_backups$ ls
backups  training_bkp2.tar  training_bkp.sql

isx48062351@j28:~/exercicis_backups$ file training_bkp2.tar
training_bkp2.tar: POSIX tar archive
```

## Pregunta 4.

El mateix d'abans però ara el destí és un fitxer tar però la base de dades és mooolt gran.

I com restauraries la base de dades a partir del fitxer que obtens a la pregunta anterior?

```
isx48062351@j28:~/exercicis_backups$ pg_dump -F t training | gzip > training_bkp3.tar.gz

isx48062351@j28:~/exercicis_backups$ ls -la
total 60
drwxr-xr-x  3 isx48062351 hisx2  4096 Mar 25 08:31 .
drwx--x--x 21 isx48062351 hisx2  4096 Mar 25 08:30 ..
drwxr-xr-x  2 isx48062351 hisx2  4096 Mar 25 08:18 backups
-rw-r--r--  1 isx48062351 hisx2     0 Mar 25 08:28 tar
-rw-r--r--  1 isx48062351 hisx2 26112 Mar 25 08:28 training_bkp2.tar
-rw-r--r--  1 isx48062351 hisx2  4425 Mar 25 08:31 training_bkp3.tar.gz
-rw-r--r--  1 isx48062351 hisx2  9876 Mar 25 08:17 training_bkp.sql

isx48062351@j28:~/exercicis_backups$ gunzip -c -k training_bkp3.tar.gz | pg_restore -C -f=training_bkp3.tar
```

## Pregunta 5

Imagina que vols fer la mateixa ordre d'abans però vols optimitzar el temps
d'execució. No pots esperar tant de temps en fer el backup. Quina ordre
aconseguirà treballar en paral·lel? Fes la mateixa ordre d'abans però atacant
la info de les 5 taules de la base de dades al mateix temps.

```
isx48062351@j28:~/exercicis_backups$ pg_dump -F t training -j 5 | gzip > training_bkp4.tar.gz
pg_dump: error: parallel backup only supported by the directory format

NO PODREM FER-LA JA QUE COM BÉ ENS POSA, AQUESTA ORDRE NOMÉS SOPORTA EL FORMAT 'd' --> DIRECTOIRS!
```

## Pregunta 6

Si no indiquem usuari ni tipus de fitxer quin és el valor per defecte?

```
isx48062351@j28:~/exercicis_backups$ pg_dump --dbname=training --file=training_bkp4.sql

isx48062351@j28:~/exercicis_backups$ ls -la
total 72
drwxr-xr-x  3 isx48062351 hisx2  4096 Mar 25 08:37 .
drwx--x--x 21 isx48062351 hisx2  4096 Mar 25 08:34 ..
drwxr-xr-x  2 isx48062351 hisx2  4096 Mar 25 08:18 backups
-rw-r--r--  1 isx48062351 hisx2 26112 Mar 25 08:28 training_bkp2.tar
-rw-r--r--  1 isx48062351 hisx2  4425 Mar 25 08:31 training_bkp3.tar.gz
-rw-r--r--  1 isx48062351 hisx2  9876 Mar 25 08:37 training_bkp4.sql
-rw-r--r--  1 isx48062351 hisx2  9876 Mar 25 08:37 training_bkp.sql

isx48062351@j28:~/exercicis_backups$ file training_bkp4.sql
training_bkp4.sql: ASCII text
```

## Pregunta 7

Fes una còpia de seguretat lògica només de la taula _comandes_ de tipus script SQL.

```
isx48062351@j28:~/exercicis_backups$ pg_dump --help | grep "\-t, "
  -t, --table=PATTERN          dump the specified table(s) only

isx48062351@j28:~/exercicis_backups$ pg_dump --dbname=training -t comandes --username=isx48062351 --format=plain --file=training_bkp5.sql

isx48062351@j28:~/exercicis_backups$ ls -la
total 76
drwxr-xr-x  3 isx48062351 hisx2  4096 Mar 25 08:40 .
drwx--x--x 21 isx48062351 hisx2  4096 Mar 25 08:34 ..
drwxr-xr-x  2 isx48062351 hisx2  4096 Mar 25 08:18 backups
-rw-r--r--  1 isx48062351 hisx2 26112 Mar 25 08:28 training_bkp2.tar
-rw-r--r--  1 isx48062351 hisx2  4425 Mar 25 08:31 training_bkp3.tar.gz
-rw-r--r--  1 isx48062351 hisx2  9876 Mar 25 08:37 training_bkp4.sql
-rw-r--r--  1 isx48062351 hisx2  3289 Mar 25 08:40 training_bkp5.sql
-rw-r--r--  1 isx48062351 hisx2  9876 Mar 25 08:37 training_bkp.sql
```

## Pregunta 8

Fes una còpia de seguretat lògica només de la taula _rep_vendes_ de tipus binari.

```
isx48062351@j28:~/exercicis_backups$ pg_dump --dbname=training -t rep_vendes --username=isx48062351 --format=custom --file=training_bkp6.bin

isx48062351@j28:~/exercicis_backups$ ls -la
total 80
drwxr-xr-x  3 isx48062351 hisx2  4096 Mar 25 08:52 .
drwx--x--x 21 isx48062351 hisx2  4096 Mar 25 08:34 ..
drwxr-xr-x  2 isx48062351 hisx2  4096 Mar 25 08:18 backups
-rw-r--r--  1 isx48062351 hisx2 26112 Mar 25 08:28 training_bkp2.tar
-rw-r--r--  1 isx48062351 hisx2  4425 Mar 25 08:31 training_bkp3.tar.gz
-rw-r--r--  1 isx48062351 hisx2  9876 Mar 25 08:37 training_bkp4.sql
-rw-r--r--  1 isx48062351 hisx2  3289 Mar 25 08:42 training_bkp5.sql
-rw-r--r--  1 isx48062351 hisx2  3330 Mar 25 08:51 training_bkp6.bin
-rw-r--r--  1 isx48062351 hisx2  9876 Mar 25 08:37 training_bkp.sql
```

## Pregunta 9

Elimina la taula _comandes_ de la base de dades _training_.

Quina ordre restaura la taula *comandes*:

```
training=> \d
             List of relations
 Schema |    Name    | Type  |    Owner
--------+------------+-------+-------------
 public | clients    | table | isx48062351
 public | comandes   | table | isx48062351
 public | oficines   | table | isx48062351
 public | productes  | table | isx48062351
 public | rep_vendes | table | isx48062351
(5 rows)

training=> DROP TABLE comandes;
DROP TABLE

training=> \d
             List of relations
 Schema |    Name    | Type  |    Owner
--------+------------+-------+-------------
 public | clients    | table | isx48062351
 public | oficines   | table | isx48062351
 public | productes  | table | isx48062351
 public | rep_vendes | table | isx48062351
(4 rows)

isx48062351@j28:~/exercicis_backups$ psql --dbname=training --username=isx48062351 < training_bkp5.sql

training=# \d
             List of relations
 Schema |    Name    | Type  |    Owner
--------+------------+-------+-------------
 public | clients    | table | isx48062351
 public | comandes   | table | isx48062351
 public | oficines   | table | isx48062351
 public | productes  | table | isx48062351
 public | rep_vendes | table | isx48062351
(5 rows)
```

## Pregunta 10

Fes un cop d'ull al backup tar creat a l'exercici 3 (per exemple amb l'ordre tar xvf ). Creus que a partir d'aquest fitxer es podria restaurar només una taula? Si és així, escriu i comprova-ho amb rep_vendes (abans hauràs d'eliminar la taula amb DROP)

```
isx48062351@j28:~/exercicis_backups$ tar xvf training_bkp2.tar
toc.dat
3020.dat
3021.dat
3018.dat
3017.dat
3019.dat
restore.sql

isx48062351@j28:~/exercicis_backups$ ls -la \*.dat restore\*
-rw------- 1 isx48062351 hisx2  821 Mar 25 08:28 3017.dat
-rw------- 1 isx48062351 hisx2  206 Mar 25 08:28 3018.dat
-rw------- 1 isx48062351 hisx2  729 Mar 25 08:28 3019.dat
-rw------- 1 isx48062351 hisx2  671 Mar 25 08:28 3020.dat
-rw------- 1 isx48062351 hisx2 1427 Mar 25 08:28 3021.dat
-rw------- 1 isx48062351 hisx2 7328 Mar 25 08:28 restore.sql
-rw------- 1 isx48062351 hisx2 8266 Mar 25 08:28 toc.dat

training=# \d
             List of relations
 Schema |    Name    | Type  |    Owner
--------+------------+-------+-------------
 public | clients    | table | isx48062351
 public | comandes   | table | isx48062351
 public | oficines   | table | isx48062351
 public | productes  | table | isx48062351
 public | rep_vendes | table | isx48062351
(5 rows)

training=# DROP TABLE rep_vendes CASCADE;
NOTICE:  drop cascades to 3 other objects
DETAIL:  drop cascades to constraint fk_clients_rep_clie on table clients
drop cascades to constraint fk_comandes_rep on table comandes
drop cascades to constraint fk_oficina_director on table oficines
DROP TABLE

training=# \d
            List of relations
 Schema |   Name    | Type  |    Owner
--------+-----------+-------+-------------
 public | clients   | table | isx48062351
 public | comandes  | table | isx48062351
 public | oficines  | table | isx48062351
 public | productes | table | isx48062351
(4 rows)

isx48062351@j28:~/exercicis_backups$ pg_restore -d training -Ft -t rep_vendes < fitxer.tar

training=# \d
             List of relations
 Schema |    Name    | Type  |    Owner
--------+------------+-------+-------------
 public | clients    | table | isx48062351
 public | comandes   | table | isx48062351
 public | oficines   | table | isx48062351
 public | productes  | table | isx48062351
 public | rep_vendes | table | isx48062351
(5 rows)
```

## Pregunta 11

Elimina la taula *rep_vendes* de la base de dades training.

Quina ordre restaura la taula *rep_vendes* (recorda que el fitxer és binari)

```
training=# \d
             List of relations
 Schema |    Name    | Type  |    Owner
--------+------------+-------+-------------
 public | clients    | table | isx48062351
 public | comandes   | table | isx48062351
 public | oficines   | table | isx48062351
 public | productes  | table | isx48062351
 public | rep_vendes | table | isx48062351
(5 rows)

training=# DROP TABLE rep_vendes CASCADE;
NOTICE:  drop cascades to 3 other objects
DETAIL:  drop cascades to constraint fk_clients_rep_clie on table clients
drop cascades to constraint fk_oficina_director on table oficines
drop cascades to constraint fk_comandes_rep on table comandes
DROP TABLE

training=# \d
            List of relations
 Schema |   Name    | Type  |    Owner
--------+-----------+-------+-------------
 public | clients   | table | isx48062351
 public | comandes  | table | isx48062351
 public | oficines  | table | isx48062351
 public | productes | table | isx48062351
(4 rows)

isx48062351@j28:~/exercicis_backups$ pg_restore -Fc -d training training_bkp6.bin

training=# \d
             List of relations
 Schema |    Name    | Type  |    Owner
--------+------------+-------+-------------
 public | clients    | table | isx48062351
 public | comandes   | table | isx48062351
 public | oficines   | table | isx48062351
 public | productes  | table | isx48062351
 public | rep_vendes | table | isx48062351
(5 rows)
```

## Pregunta 12

Després de fer una còpia de tota la base de dades training en format binari, elimina-la.

Hi ha l'ordre de bash `dropdb` (en realitat és un wrapper de DROP DATABASE). I de manera anàloga també n'hi ha l'ordre de bash `createdb`.

Sense utilitzar aquesta última i amb una única ordre restaura la base de dades training.

```
isx48062351@j28:~/exercicis_backups$ pg_dump --dbname=training format=custom --file=training_bkp7.bin

isx48062351@j28:~/exercicis_backups$ ls -la
total 92
drwxr-xr-x  3 isx48062351 hisx2  4096 Mar 25 09:08 .
drwx--x--x 21 isx48062351 hisx2  4096 Mar 25 08:34 ..
drwxr-xr-x  2 isx48062351 hisx2  4096 Mar 25 08:18 backups
-rw-r--r--  1 isx48062351 hisx2 26112 Mar 25 08:28 training_bkp2.tar
-rw-r--r--  1 isx48062351 hisx2  4425 Mar 25 08:31 training_bkp3.tar.gz
-rw-r--r--  1 isx48062351 hisx2  9876 Mar 25 08:37 training_bkp4.sql
-rw-r--r--  1 isx48062351 hisx2  3289 Mar 25 08:42 training_bkp5.sql
-rw-r--r--  1 isx48062351 hisx2  3330 Mar 25 08:51 training_bkp6.bin
-rw-r--r--  1 isx48062351 hisx2  9013 Mar 25 09:08 training_bkp7.bin
-rw-r--r--  1 isx48062351 hisx2  9876 Mar 25 08:37 training_bkp.sql

isx48062351@j28:~/exercicis_backups$ dropdb training

isx48062351@j28:~/exercicis_backups$ pg_restore -C -f training_bkp7.bin

training=# \d
             List of relations
 Schema |    Name    | Type  |    Owner
--------+------------+-------+-------------
 public | clients    | table | isx48062351
 public | comandes   | table | isx48062351
 public | oficines   | table | isx48062351
 public | productes  | table | isx48062351
 public | rep_vendes | table | isx48062351
(5 rows)
```

## Pregunta 13

Per defecte, si n'hi ha un error quan fem la restauració psql ignora l'error i continuarà executant-se. Si volem que pari just quan hi hagi l'errada quina opció afegirem a l'ordre psql:

```
isx48062351@j28:~/exercicis_backups$ pg_restore -Fc -d -e training training_bkp8.bin

-e (exit-on-error) --> A l'hora que envia les comandes sql, ens envía un error.
```

## Pregunta 14

Si es vol que la restauració es faci en mode transacció, és a dir o es fa tot o no es fa res, com seria l'ordre?

```
isx48062351@j28:~/exercicis_backups$ pg_restore -aC backup.sql
```

## Pregunta 15

Quina ordre em permet fer un backup de totes les bases de dades d'un cluster? Es necessita ser l'administrador de la base de dades?

```
isx48062351@j28:~/exercicis_backups$ pg_dumpall > backup_all_db.sql

Sí, és necessita ser administrador.
```

## Pregunta 16

Quina ordre em permet restaurar totes les bases de dades d'un cluster a partir del fitxer backup_all_db.sql? Es necessita ser l'administrador de la base de dades?

```
isx48062351@j28:~/exercicis_backups$ psql < backup_all_db.sql

Sí, és necessita ser administrador.
```

## Pregunta 17

Quina ordre em permet fer una còpia de seguretat de tipus tar la base de dades training que es troba a 192.168.0.123 ? Canvia la IP per la de l'ordinador del teu company.

```
isx48062351@j28:~/exercicis_backups$ pg_dump -h 10.200.244.229 -U isx48062351 -F t training > training_bkp_company.tar

isx48062351@j28:~/exercicis_backups$ tar tf training_bkp_company.tar
toc.dat
3020.dat
3021.dat
3018.dat
3017.dat
3019.dat
restore.sql
```

## Pregunta 18

Es pot fer un backup de la base de dades training del server1 i que es restauri on the fly a server2?

```
isx48062351@j28:~/exercicis_backups$ pg_dump -h 10.200.244.229 training > training_bkp2_company.sql || psql training < training_bkp2_company.sql

isx48062351@j28:~/exercicis_backups$ cat training_bkp2_company.sql | less
--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3 (Debian 13.3-1)
-- Dumped by pg_dump version 13.4 (Debian 13.4-0+deb11u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';
...
:
```
