 -- --------------------------------------------
--  Tipus de dades
-- --------------------------------------------

-- 7.1- Raona un parell d'exemples en els que creus que se usaría el tipus
-- de dades següents.

-- boolean → Per veure valors de T/F (True/False)

-- character varying → Per veure la longitud de caràcters amb límits.

-- character → Per veure la longitud de caràcters

-- date → Per veure la data.

-- integer → Per fer operacions amb números enters positius i per comptar alguna cosa.

-- interval → Per comptar coses amb intervals de temps (diferents dades de temps)

-- money → Per comptar diners

-- numeric → Per comptar / treure valor de qualsevol número (float, negatiu, enter…)

-- path →Per treure el path d’una figura geomètrica.

-- point → Per treure el point d’una figura geomètrica.

-- polygon → Per polígons.

-- real → Per números reals.

-- smallint → Per números enters.

-- serial → Per incrementar ‘X’ valors

-- text → Per escriure un paràgraf o un troç d’un llibre.

-- time → Per veure l’hora sense zona horària.

-- timestamp → Per veure data i hora sense la zona horària.

-- uuid → Per veure l’UUID

-- xml → Per guardar dades i per validar estructura de dades.

-- 7.2- Raona quin tipus de dades utilitzaries per emmagatzemar la següent
-- informació. Escriu la notació SQL per declarar els camps. En cas de
-- necessitar un tipus compost escriu la sentencia SQL necessària per a
-- crear aquest tipus de dades.

-- Adreça MAC i adreça IPv4. → macaddr

-- Dia i hora d'una videoconferència intercontinental. → timestamp

-- Si hi ha o no televisió en una habitació d'hotel. → polygon

-- Dos números de telèfon, un de la feina, un de particular. → integer

-- Lletra i duració d'una cançó. → text + interval

-- Recorregut d'una carretera. → interval

-- Cobertura d'un radar meteorològic. → numeric

-- Data d'un aniversari. →date

-- Despesa d'un viatge de negocis. → integer

-- Matrícula de cotxe. → text

-- Illa de cases en un mapa. → integer

-- Localització i alçada del pic d'una muntanya. → numeric

-- Identificador de xarxa IPv6. → text

-- Hora del dia en que s'ha de realitzar una tasca. → time

-- Document Nacional d'Identitat. → text

-- Nombre de llits d'un Hospital. → integer / smallint

-- Document XHTML i URL. → xml

-- Nombre d'escales, de plantes i pisos d'un edifici. → integer

-- Abast geogràfic d'un mapa. → numeric

-- Segons durant els que un semàfor està en verd, vermell i ambre. → interval
