# Exercicis DDL (Estructura)

## Exercici 1

Crear una base de dades anomenada "biblioteca". Dins aquesta base de dades:

+ crear una taula anomenada llibres amb els camps següents:
	+ "ref": clau primaria numèrica que identifica els llibres i s'ha d'assignar automàticament.
	+ "titol": títol del llibre.
	+ "editorial": nom de l'editorial.
	+ "autor": identificador de l'autor, clau forana, per defecte ha de
+ referenciar a primer valor de la taula autors que simbolitzar l'autor "Anònim".
+ Crear una altre taula anomenada "autors" amb els següents camps:
	+ "autor": identificador de l'autor.
	+ "nom": Nom i cognoms de l'autor.
	+ "nacionalitat": Nacionalitat de l'autor.
+ Ambdues taules han de mantenir integritat referencial. Cal que si es trenca
+ la integritat per delete d'autor, la clau forana del llibre apunti a "Anònim".
+ Si es trenca la integritat per insert/update s'ha d'impedir l'alta/modificació.
+ Cal inserir l'autor "Anònim" a la taula autors.

template1=> CREATE DATABASE biblioteca;

CREATE DATABASE

template1=> \c biblioteca;

You are now connected to database "biblioteca" as user "isx48062351".

biblioteca=>

biblioteca=> START TRANSACTION;

START TRANSACTION

```
biblioteca=*> CREATE TABLE llibres (
                      PRIMARY KEY (ref),
                      ref            SERIAL       NOT NULL,
                      titol          VARCHAR(30),
                      editorial      VARCHAR(30),
                      autor          INT          DEFAULT 1
                                                  REFERENCES autors
                                                  ON DELETE SET DEFAULT
                                                  ON UPDATE RESTRICT
);
CREATE TABLE
```

biblioteca=*> COMMIT;

biblioteca=> START TRANSACTION;

START TRANSACTION

```
biblioteca=*> CREATE TABLE autors (
                      PRIMARY KEY (autor),
                      autor           INT          NOT NULL,
                      nom             VARCHAR(30),
                      nacionalitat    VARCHAR(20)
);
CREATE TABLE
```

```
biblioteca=*> INSERT INTO autors
              VALUES (1, 'Anònim', 'Anònim');
INSERT 0 1
```

biblioteca=*> COMMIT;

## Exercici 2

A la base biblioteca cal crear una taula anomenada "socis"

+ amb els següents camps:
	+ num_soci: clau primària
	+ nom: nom i cognoms del soci.
	+ dni: DNI del soci.
+ També una taula anomenada préstecs amb els següents camps:
	+ ref: clau forana, que fa referència al llibre prestat.
	+ soci: clau forana, que fa referència al soci.
	+ data_prestec: data en que s'ha realitzat el préstec.
+ No cal que préstecs tingui clau principal ja que només és una taula de relació.
+ En eliminar un llibre cal que s'eliminin els seus préstecs automàticament.
+ No s'ha de poder eliminar un soci amb préstecs pendents

biblioteca=> START TRANSACTION;

START TRANSACTION

```
biblioteca=*> CREATE TABLE socis(
                      PRIMARY KEY(num_soci),
                      num_soci           SERIAL,
                      nom                VARCHAR(30),
                      dni                VARCHAR(9)
);
```

```
biblioteca=*> CREATE TABLE prestecs(
                      ref                SERIAL        REFERENCES llibres ON DELETE CASCADE,
                      soci               SERIAL        REFERENCES socis ON DELETE RESTRICT,
                      data_prestec       DATE
);
```

biblioteca=*> COMMIT;

## Exercici 3

A la base de dades training crear una taula anomenada "rep_vendes_baixa" que tingui la mateixa estructura que la taula rep_vendes i a més a més un camp anomenat "baixa" que pugui contenir la data en que un representant de vendes s'ha donat de baixa.

training=> START TRANSACTION;

START TRANSACTION

```
training=*> CREATE TABLE rep_vendes_baixa (
                    baixa       VARCHAR(10)
             ) INHERITS (rep_vendes);
CREATE TABLE
```
```
training=*> SELECT * FROM rep_vendes_baixa;
 num_empl | nom | edat | oficina_rep | carrec | data_contracte | cap | quota | vendes | baixa 
----------+-----+------+-------------+--------+----------------+-----+-------+--------+-------
(0 rows)
```
training=*> COMMIT;

COMMIT

## Exercici 4

A la base de dades training crear una taula anomenada "productes_sense_comandes" omplerta amb aquells productes que no han tingut mai cap comanda. A continuació esborrar de la taula "productes" aquells productes que estan en aquesta nova taula.

training=> START TRANSACTION;

START TRANSACTION

```
training=*> CREATE TABLE productes_sense_comandes
                AS (SELECT *
                      FROM productes
                     WHERE id_producte NOT IN (SELECT producte
                                                   FROM comandes));
SELECT 8
```

```
training=*> SELECT * FROM productes_sense_comandes;
 id_fabricant | id_producte
--------------+-------------
 bic          | 41672
 imm          | 887p
 qsa          | xk48
 imm          | 887h
 bic          | 41089
 aci          | 41001
 qsa          | xk48a
 imm          | 887x
(8 rows)
```
```
training=*> DELETE FROM productes
             WHERE id_producte IN (SELECT id_producte
                                     FROM productes_sense_comandes);
DELETE 8
```
training=*> COMMIT;

## Exercici 5

A la base de dades training crear una taula temporal que substitueixi la taula "clients" però només ha de contenir aquells clients que no han fet comandes i tenen assignat un representant de vendes amb unes vendes inferiors al 110% de la seva quota.

training=> START TRANSACTION;

START TRANSACTION

```
training=*> CREATE LOCAL TEMPORARY TABLE clients2
                AS (SELECT rep_clie
                      FROM clients
                      JOIN rep_vendes
                        ON clients.rep_clie = rep_vendes.num_empl
                     WHERE rep_vendes.vendes * 1.1 < rep_vendes.quota
                       AND num_clie NOT IN (SELECT clie
                                              FROM comandes));
SELECT 0
```

```
training=*# SELECT * FROM clients2;
 rep_clie
----------
(0 rows)
```
training=*> COMMIT;

## Exercici 6

Escriu les sentències necessàries per a crear l'estructura de l'esquema proporcionat de la base de dades training. Justifica les accions a realitzar en modificar/actualitzar les claus primàries.

```
CREATE TABLE productes (
        PRIMARY KEY (id_fabricant, id_producte),
        id_fabricant        VARCHAR(3),
        id_producte         VARCHAR(5),
        descripcio          VARCHAR(20)          NOT NULL,
        preu                NUMERIC(8,2)         NOT NULL,
        estoc               INT                  DEFAULT 0
);
```

```
CREATE TABLE oficines (
        PRIMARY KEY (oficina),
        oficina             SMALLINT,
        ciutat              VARCHAR(15)          NOT NULL,
        regio               VARCHAR(10)          NOT NULL,
        director            SMALLINT,
        objectiu            NUMERIC(8,2),
        vendes              NUMERIC(8,2)
);
```

```
CREATE TABLE rep_vendes (
        PRIMARY KEY(num_empl),
        num_empl            SMALLINT,
        nom                 VARCHAR(20)          NOT NULL,
        edat                SMALLINT,
        oficina_rep         SMALLINT,
        carrec              VARCHAR(15),
        data_contracte      DATE                 NOT NULL,
        cap                 SMALLINT,
        quota               NUMERIC(8,2)         DEFAULT 250000.00,
        vendes              NUMERIC(8,2)         DEFAULT 0,
);
```

```
CREATE TABLE clients (
        PRIMARY KEY(num_clie),
        num_clie            SMALLINT,
        empresa             VARCHAR(20)         NOT NULL,
        rep_clie            SMALLINT            NOT NULL,
        limit_credit        NUMERIC(8,2)        DEFAULT 0,
);
```

```
CREATE TABLE comandes (
        PRIMARY KEY(num_comanda),
        num_comanda         INT,
        data                DATE                NOT NULL,
        clie                SMALLINT            NOT NULL,
        rep                 SMALLINT,
        fabricant           VARCHAR(3)          NOT NULL,
        producte            VARCHAR(5)          NOT NULL,
        quantitat           SMALLINT            NOT NULL,
        import              NUMERIC(7,2)        NOT NULL DEFAULT 0,
);
```

## Exercici 7

Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que afegeixi un camp anomenat "nif" a la taula "clients" que permeti emmagatzemar el NIF de cada client. També s'ha de procurar que el NIF de cada client sigui únic.

training=> START TRANSACTION;

START TRANSACTION

```
training=*> ALTER TABLE clients
              ADD nif VARCHAR(9) UNIQUE;
ALTER TABLE
```

```
training=*> SELECT * FROM clients;
 num_clie |      empresa      | rep_clie | limit_credit | nif
----------+-------------------+----------+--------------+-----
     2111 | JCP Inc.          |      103 |     50000.00 |
     2102 | First Corp.       |      101 |     65000.00 |
...

(21 rows)
```

```
training=*> \d clients
                        Table "public.clients"
    Column    |         Type          | Collation | Nullable | Default 
--------------+-----------------------+-----------+----------+---------
 num_clie     | smallint              |           | not null | 
 empresa      | character varying(20) |           | not null | 
 rep_clie     | smallint              |           | not null | 
 limit_credit | numeric(8,2)          |           |          | 
 nif          | character varying(9)  |           |          | 
Indexes:
    "pk_clients_num_clie" PRIMARY KEY, btree (num_clie)
    "clients_nif_key" UNIQUE CONSTRAINT, btree (nif)
Foreign-key constraints:
    "fk_clients_rep_clie" FOREIGN KEY (rep_clie) REFERENCES rep_vendes(num_empl)
Referenced by:
    TABLE "comandes" CONSTRAINT "fk_comandes_clie" FOREIGN KEY (clie) REFERENCES clients(num_clie)
```

training=*> COMMIT;

## Exercici 8

Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que afegeixi un camp anomenat "tel" a la taula "clients" que permeti emmagatzemar el número de telèfon de cada client. També s'ha de procurar que aquest contingui 9 xifres.

training=> START TRANSACTION;

START TRANSACTION

```
training=*> ALTER TABLE clients
              ADD tel INT;
ALTER TABLE
```

```
training=*> ALTER TABLE clients
              ADD CONSTRAINT clients_tel_ck CHECK(length(tel::text) = 9);
ALTER TABLE
```

```
training=*> SELECT * FROM clients;
 num_clie |      empresa      | rep_clie | limit_credit | tel
----------+-------------------+----------+--------------+-----
     2111 | JCP Inc.          |      103 |     50000.00 |     |
     2102 | First Corp.       |      101 |     65000.00 |     |
...

(21 rows)
```

```
training=*> \d clients
                        Table "public.clients"
    Column    |         Type          | Collation | Nullable | Default
--------------+-----------------------+-----------+----------+---------
 num_clie     | smallint              |           | not null |
 empresa      | character varying(20) |           | not null |
 rep_clie     | smallint              |           | not null |
 limit_credit | numeric(8,2)          |           |          |
 tel          | integer               |           |          |
Indexes:
    "pk_clients_num_clie" PRIMARY KEY, btree (num_clie)
Check constraints:
    "clients_tel_ck" CHECK (length(tel::text) = 9)
Foreign-key constraints:
    "fk_clients_rep_clie" FOREIGN KEY (rep_clie) REFERENCES rep_vendes(num_empl)
Referenced by:
    TABLE "comandes" CONSTRAINT "fk_comandes_clie" FOREIGN KEY (clie) REFERENCES clients(num_clie)
```

training=*> COMMIT;

## Exercici 9

Escriu les sentències necessàries per modificar la base de dades training proporcionada. Cal que s'impedeixi que els noms dels representants de vendes i els noms dels clients estiguin buits, és a dir que ni siguin nuls ni continguin la cadena buida.

training=> START TRANSACTION;

START TRANSACTION

```
training=*> ALTER TABLE clients
              ADD CONSTRAINT clients_empresa_ck CHECK ( empresa <> '' OR empresa IS NOT NULL );
```

```
training=*> ALTER TABLE rep_vendes
              ADD CONSTRAINT rep_vendes_nom_ck CHECK ( nom <> '' OR nom IS NOT NULL );
```

training=*> COMMIT;

## Exercici 10

Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que procuri que l'edat dels representants de vendes no sigui inferior a 18 anys ni superior a 65.

training=> START TRANSACTION;

START TRANSACTION

```
training=*> ALTER TABLE rep_vendes
              ADD CONSTRAINT rep_vendes_edat_ck CHECK ( edat >= 18 AND edat <= 65 );
```

training=*> COMMIT;

## Exercici 11

Escriu una sentència que permeti modificar la base de dades training proporcionada. Cal que esborri el camp "carrec" de la taula "rep_vendes" esborrant també les possibles restriccions i referències que tingui.

training=> START TRANSACTION;

START TRANSACTION

```
training=*> ALTER TABLE rep_vendes
             DROP carrec CASCADE;
```

training=*> COMMIT;

## Exercici 12

Escriu les sentències necessàries per modificar la base de dades training proporcionada per tal que aquesta tingui integritat referencial. Justifica les accions a realitzar per modificar les dades.

```
CREATE TABLE productes (
        PRIMARY KEY (id_fabricant, id_producte),
        id_fabricant        VARCHAR(3),
        id_producte         VARCHAR(5),
        descripcio          VARCHAR(20)          NOT NULL,
        preu                NUMERIC(8,2)         NOT NULL,
        estoc               INT                  DEFAULT 0
);
```

```
CREATE TABLE oficines (
        PRIMARY KEY (oficina),
        oficina             SMALLINT,
        ciutat              VARCHAR(15)          NOT NULL,
        regio               VARCHAR(10)          NOT NULL,
        director            SMALLINT,
        objectiu            NUMERIC(8,2),
        vendes              NUMERIC(8,2)
);
```

```
CREATE TABLE rep_vendes (
        PRIMARY KEY(num_empl),
        num_empl            SMALLINT,
        nom                 VARCHAR(20)          NOT NULL,
        edat                SMALLINT,
        oficina_rep         SMALLINT,
        carrec              VARCHAR(15),
        data_contracte      DATE                 NOT NULL,
        cap                 SMALLINT,
        quota               NUMERIC(8,2)         DEFAULT 250000.00,
        vendes              NUMERIC(8,2)         DEFAULT 0,
                            CONSTRAINT CK_REP_VENDES_NOM CHECK(NOM = INITCAP(NOM)),
    	                    CONSTRAINT CK_REP_VENDES_EDAT CHECK(EDAT>0),
    	                    CONSTRAINT CK_REP_VENDES_QUOTA CHECK(QUOTA>0),
    	                    CONSTRAINT FK_REP_VENDES_OFICINA_REP FOREIGN KEY(oficina_rep)
                            REFERENCES OFICINES(oficina)
);
```

```
CREATE TABLE clients (
        PRIMARY KEY(num_clie),
        num_clie            SMALLINT,
        empresa             VARCHAR(20)         NOT NULL,
        rep_clie            SMALLINT            NOT NULL,
        limit_credit        NUMERIC(8,2)        DEFAULT 0,
                            CONSTRAINT FK_CLIENTS_REP_CLIE FOREIGN KEY(REP_CLIE) REFERENCES 
                            REP_VENDES(NUM_EMPL)
);
```

```
CREATE TABLE comandes (
        PRIMARY KEY(num_comanda),
        num_comanda         INT,
        data                DATE                NOT NULL,
        clie                SMALLINT            NOT NULL,
        rep                 SMALLINT,
        fabricant           VARCHAR(3)          NOT NULL,
        producte            VARCHAR(5)          NOT NULL,
        quantitat           SMALLINT            NOT NULL,
        import              NUMERIC(7,2)        NOT NULL DEFAULT 0,
    	                    CONSTRAINT FK_COMANDES_REP FOREIGN KEY(REP) REFERENCES 
                            REP_VENDES(NUM_EMPL),
    	                    CONSTRAINT FK_COMANDES_CLIE FOREIGN KEY(CLIE) REFERENCES 
                            CLIENTS(NUM_CLIE)
);
```
