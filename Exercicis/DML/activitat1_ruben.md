# Modificació

## Exercici 1:

Inseriu un nou venedor amb nom "Enric Jimenez" amb identificador 1012, oficina 12, títol "Dir Vendes", contracte d'1 de febrer del 2012, cap 101 i vendes 0.

Primer, hem d'eliminar la constraint de que les vendes siguin major a 0.

```
training=#  ALTER TABLE rep_vendes DROP CONSTRAINT ck_rep_vendes_vendes; 
```

```
training=# INSERT INTO rep_vendes 
           VALUES ( 1012, 'Enric Jimenez', NULL, 12, 'Dir Vendes', '2012-02-01', 101, NULL, 0);
INSERT 0 1
```

## Exercici 2:

Inseriu un nou client "C1" i una nova comanda pel venedor anterior.

```
training=# INSERT INTO clients 
           VALUES ( 2125, 'C1', 105, NULL);
INSERT 0 1
```

```
training=# INSERT INTO comandes 
           VALUES ( 114001, '1990-02-19', 2125, 105, 'imm', '775c', 25, 3600);
INSERT 0 1
```

## Exercici 3:

Inseriu un nou venedor amb nom "Pere Mendoza" amb identificador 1013, contracte del 15 de agost del 2011 i vendes 0. La resta de camps a null.

```
training=# INSERT INTO rep_vendes (num_empl, nom, data_contracte, vendes)
           VALUES ( 1013, 'Pere Mendoza', '2011-08-15', 0);
INSERT 0 1
```

## Exercici 4:

Inseriu un nou client "C2" omplint els mínims camps.

```
training=# INSERT INTO clients 
           VALUES ( 2126, 'C2', 1013, NULL);
INSERT 0 1
```

## Exercici 5:

Inseriu una nova comanda del client "C2" al venedor "Pere Mendoza" sense especificar la llista de camps pero si la de valors.

```
training=# INSERT INTO comandes 
           VALUES ( 114002, '1989-11-24', 2126, 1013, 'aci', '41003', 10, 14000);
INSERT 0 1
```

## Exercici 6:

Esborreu de la còpia de la base de dades el venedor afegit anteriorment anomenat "Enric Jimenez".

```
training=# DELETE FROM rep_vendes 
            WHERE nom = 'Enric Jimenez';
DELETE 1
```

## Exercici 7:

Elimineu totes les comandes del client "C1" afegit anteriorment.

```
training=# DELETE FROM comandes 
            WHERE clie = 2125;
DELETE 1
```

ALTRA FORMA:

```
training=# DELETE FROM comandes 
            WHERE clie = ANY 
                  (SELECT num_clie 
                     FROM clients 
                    WHERE empresa = 'C1');
DELETE 1
```

## Exercici 8:

Esborreu totes les comandes d'abans del 15-11-1989.

```
training=# DELETE FROM comandes 
            WHERE data < '1989-11-15';
DELETE 4
```

## Exercici 9:

Esborreu tots els clients dels venedors: Adams, Jones i Robe	rts.

```
training=# DELETE FROM clients 
            WHERE rep_clie IN 
                  (SELECT num_empl 
                     FROM rep_vendes 
                    WHERE nom LIKE '% Adams' OR nom LIKE '% Jones' OR nom LIKE '% Roberts');
ERROR:  update or delete on table "clients" violates foreign key constraint "fk_comandes_clie" on table "comandes"
DETAIL:  Key (num_clie)=(2103) is still referenced from table "comandes".
```

## Exercici 10:

Esborreu tots els venedors contractats abans del juliol del 1988 que encara no se'ls ha assignat una quota.

```
training=# DELETE FROM rep_vendes 
            WHERE extract('year' FROM data_contracte) < '1988' AND extract('month' FROM data_contracte) < '07' AND quota IS NULL;
DELETE 0
```

## Exercici 11:

Esborreu totes les comandes.

```
training=# DELETE FROM comandes;
DELETE 27
```

## Exercici 12:

Esborreu totes les comandes acceptades per la Sue Smith (cal tornar a disposar de la taula comandes)

```
training=# \i '/home/users/inf/hisx2/isx48062351/Downloads/trainingv4.sql'
ALTER TABLE
ALTER TABLE
DROP TABLE
DROP TABLE
DROP TABLE
DROP TABLE
DROP TABLE
CREATE TABLE
CREATE TABLE
CREATE TABLE
CREATE TABLE
CREATE TABLE
COPY 25
COPY 5
COPY 10
COPY 21
COPY 30
ALTER TABLE
ALTER TABLE
```

```
training=# DELETE FROM comandes 
            WHERE rep IN 
                  (SELECT num_empl 
                     FROM rep_vendes 
                    WHERE nom = 'Sue Smith');
DELETE 4
```

## Exercici 13:

Suprimeix els clients atesos per venedors les vendes dels quals són inferiors al 80% de la seva quota.

```
training=# DELETE FROM clients 
            WHERE rep_clie IN 
                  (SELECT num_empl 
                     FROM rep_vendes 
                    WHERE vendes < 0.8 * quota);
ERROR:  update or delete on table "clients" violates foreign key constraint "fk_comandes_clie" on table "comandes"
DETAIL:  Key (num_clie)=(2124) is still referenced from table "comandes".
```

## Exercici 14:

Suprimiu els venedors els quals el seu total de comandes actual (imports) és menor que el 2% de la seva quota.

```
training=# DELETE FROM rep_vendes 
            WHERE quota * 0.02 > 
                  (SELECT SUM(import) 
                     FROM comandes 
                    WHERE num_empl = rep);
ERROR:  update or delete on table "rep_vendes" violates foreign key constraint "fk_clients_rep_clie" on table "clients"
DETAIL:  Key (num_empl)=(103) is still referenced from table "clients".
```

## Exercici 15:

Suprimiu els clients que no han realitzat comandes des del 10-11-1989.

```
training=# DELETE FROM comandes 
            WHERE clie = ANY (SELECT clie 
                                FROM comandes 
                               WHERE data < '1989-11-10');
DELETE 9
```

## Exercici 16:

Eleva el límit de crèdit de l'empresa Acme Manufacturing a 60000 i la reassignes (representant) a Mary Jones.

```
training=# UPDATE clients 
              SET limit_credit = 60000, 
                  rep_clie = (SELECT num_empl 
                                FROM rep_vendes 
                               WHERE nom = 'Mary Jones') 
            WHERE empresa LIKE 'Acme Mfg.';

UPDATE 1
```

## Exercici 17:

Transferiu tots els venedors de l'oficina de Chicago (12) a la de Nova York (11), i rebaixa les seves quotes un 10%.

```
training=# UPDATE rep_vendes 
              SET oficina_rep = (SELECT oficina 
                                   FROM oficines 
                                  WHERE ciutat = 'New York'),
                  quota = quota * 0.1 
            WHERE oficina_rep = 12;
```

## Exercici 18:

Reassigna tots els clients atesos pels empleats 105, 106, 107, a l'empleat 102.

```
training=# UPDATE clients 
              SET rep_clie = 102 
            WHERE rep_clie = 105 OR rep_clie = 106 OR rep_clie 00= 107;
UPDATE 5
```

## Exercici 19:

Assigna una quota de 100000 a tots aquells venedors que actualment no tenen quota.

```
training=# UPDATE rep_vendes 
              SET quota = 100000 
            WHERE quota IS NULL;
UPDATE 1
```

## Exercici 20:

Eleva totes les quotes un 5%.

```
training=# UPDATE rep_vendes 
              SET quota = quota * 1.05;
UPDATE 10
```

## Exercici 21:

Eleva en 5000 el límit de crèdit de qualsevol client que ha fet una comanda d'import superior a 25000.

```
training=# UPDATE clients 
              SET limit_credit = limit_credit + 5000 
            WHERE num_clie IN (SELECT clie 
                                 FROM comandes   
                                WHERE import > 25000);
UPDATE 4
```

## Exercici 22:

Reassigna tots els clients atesos pels venedors les vendes dels quals són menors al 80% de les seves quotes. Reassignar al venedor 105.

```
training=# UPDATE clients 
              SET rep_clie = 105 
            WHERE rep_clie IN (SELECT num_empl 
                                 FROM rep_vendes 
                                WHERE vendes < quota * 0.8);
UPDATE 2
```

## Exercici 23:

Feu que tots els venedors que atenen a més de tres clients estiguin sota de les ordres de Sam Clark (106).

```
training=# UPDATE rep_vendes 
              SET cap = 106 
            WHERE num_empl IN (SELECT rep 
                                 FROM comandes 
                                GROUP BY rep 
                               HAVING count(*) > 3);
UPDATE 3
```

## Exercici 24:

Augmenteu un 50% el límit de credit d'aquells clients que totes les seves comandes tenen un import major a 30000.

```
training=# UPDATE clients
              SET limit_credit = limit_credit * 1.5
            WHERE num_clie > ALL (SELECT clie
                                 FROM comandes
                                WHERE import > 30000)
               OR num_clie IS NOT NULL;
UPDATE 3
```

## Exercici 25:

Disminuiu un 2% el preu d'aquells productes que tenen un estoc superior a 200 i no han tingut comandes.

```
training=#> UPDATE productes 
              SET preu = preu * 0.02 
            WHERE estoc > 200 
              AND id_producte != ALL (SELECT producte 
                                        FROM comandes);
UPDATE 3
```

## Exercici 26:

Establiu un nou objectiu per aquelles oficines en que l'objectiu actual sigui inferior a les vendes. Aquest nou objectiu serà el doble de la suma de les vendes dels treballadors assignats a l'oficina.

```
training=# UPDATE oficines 
              SET objectiu = vendes * 2 
            WHERE objectiu < vendes;
UPDATE 3
```

## Exercici 27:

Modifiqueu la quota dels caps d'oficina que tinguin una quota superior a la quota d'algun empleat de la seva oficina. Aquests caps han de tenir la mateixa quota que l'empleat de la seva oficina que tingui una quota menor (quota més petita dels seus empleats).

```
training=# UPDATE rep_vendes 
              SET quota = (SELECT MIN(r1.quota) 
                             FROM rep_vendes AS "r1"  
                             JOIN rep_vendes AS "r2" ON r2.num_empl = r1.cap 
                            WHERE r2.quota > r1.quota
                            GROUP BY r1.num_empl)
            WHERE num_empl IN (SELECT r2.num_empl 
                                 FROM rep_vendes AS "r1"  
                                 JOIN rep_vendes AS "r2" 
                                   ON r2.num_empl = r1.cap 
                                WHERE r2.quota > r1.quota);

Hi ha un problema am l'hora de possar-li el valor a quota ja que vol agafa 2 valors.

```

## Exercici 28:

Cal que els 5 clients amb un total de compres (quantitat) més alt siguin transferits a l'empleat Tom Snyder i que se'ls augmenti el límit de crèdit en un 50%.

```
training=# UPDATE clients 
              SET rep_clie = (SELECT rep_clie 
                                FROM clients 
                                JOIN rep_vendes 
                                  ON clients.rep_clie = rep_vendes.num_empl 
                               WHERE nom = 'Tom Snyder'), 
                  limit_credit = limit_credit * 1.50 
            WHERE num_clie IN (SELECT clie 
                                 FROM comandes, clients 
                                WHERE clients.num_clie = comandes.clie 
                                ORDER BY comandes.quantitat DESC 
                                FETCH FIRST 5 ROWS ONLY);
UPDATE 4
```

## Exercici 29:

Es volen donar de baixa els productes dels que no tenen estoc i alhora no se n'ha venut cap des de l'any 89.

```
training=# DELETE FROM productes 
            WHERE estoc = 0 AND (id_fabricant, id_producte) IN 
                  (SELECT fabricant, producte 
                     FROM comandes 
                    WHERE extract('year' FROM data) > '1989');
DELETE 0
```

## Exercici 30:

Afegiu una oficina de la ciutat de "San Francisco", regió oest, el cap ha de ser "Larry Fitch", les vendes 0, l'objectiu ha de ser la mitja de l'objectiu de les oficines de l'oest i l'identificador de l'oficina ha de ser el següent valor després del valor més alt.

```
training=# INSERT INTO oficines 
           VALUES ( (SELECT MAX(oficina) + 1 
                       FROM oficines), 
                       'San Francisco', 
                       'Oest', 
                       (SELECT director 
                          FROM oficines 
                          JOIN rep_vendes 
                               ON director = num_empl 
                         WHERE nom = 'Larry Fitch' 
                         FETCH FIRST 1 ROWS ONLY), 
                               (SELECT AVG(objectiu) 
                                  FROM oficines 
                                 WHERE regio = 'Oest'), 
                        0);
INSERT 0 1
training=# SELECT * FROM oficines;
 oficina |    ciutat     | regio | director | objectiu  |  vendes
---------+---------------+-------+----------+-----------+-----------
      22 | Denver        | Oest  |      108 | 300000.00 | 186042.00
      11 | New York      | Est   |      106 | 575000.00 | 692637.00
      12 | Chicago       | Est   |      104 | 800000.00 | 735042.00
      13 | Atlanta       | Est   |      105 | 350000.00 | 367911.00
      21 | Los Angeles   | Oest  |      108 | 725000.00 | 835915.00
      23 | San Francisco | Oest  |      108 | 512500.00 |      0.00
(6 rows)
```

## BACKUP i RESTORE de la Base de DADES training:

Una manera de fer un backup ràpid des del bash és:

```
pg_dump -Fc training > training.dump
```

Podem eliminar també la base de dades:

```
dropdb training
```

Podem restaurar una base de dades amb:

```
pg_restore -C -d postgres training.dump
```

Una altra manera de fer el mateix:

Backup en fitxer pla

```
pg_dump -f training.dump training
```

Eliminem la base de dades:

```
dropdb training
```

Creem la base de dades:

```
createdb training
```

Reconstruim l'estructura i dades:

```
psql training < training.dump
```
